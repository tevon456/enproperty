<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        //Create  Permissions
        $view_property = Permission::create(['guard_name' => 'api','name' => 'view property']);
        $create_property = Permission::create(['guard_name' => 'api','name' => 'create property']);
        $edit_property = Permission::create(['guard_name' => 'api','name' => 'edit property']);
        $delete_property = Permission::create(['guard_name' => 'api','name' => 'delete property']);

        $view_listing = Permission::create(['guard_name' => 'api','name' => 'view listing']);
        $create_listing = Permission::create(['guard_name' => 'api','name' => 'create listing']);
        $edit_listing = Permission::create(['guard_name' => 'api','name' => 'edit listing']);
        $delete_listing = Permission::create(['guard_name' => 'api','name' => 'delete listing']);

        $edit_users = Permission::create(['guard_name' => 'api','name' => 'edit user']);
        $delete_users = Permission::create(['guard_name' => 'api','name' => 'delete user']);

        //Get Roles
        $user = Role::where('name','user')->first();
        $landlord = Role::where('name','landlord')->first();
        $admin = Role::where('name','admin')->first();

        //Give roles to permissions
        $user->givePermissionTo($view_listing);

        $landlord->givePermissionTo($create_listing);
        $landlord->givePermissionTo($view_listing);
        $landlord->givePermissionTo($edit_listing);
        $landlord->givePermissionTo($delete_listing);
        $landlord->givePermissionTo($create_property);
        $landlord->givePermissionTo($view_property);
        $landlord->givePermissionTo($edit_property);
        $landlord->givePermissionTo($delete_property);

        $admin->givePermissionTo($create_listing);
        $admin->givePermissionTo($view_listing);
        $admin->givePermissionTo($edit_listing);
        $admin->givePermissionTo($delete_listing);
        $admin->givePermissionTo($edit_users);
        $admin->givePermissionTo($delete_users);
        $admin->givePermissionTo($edit_property);
        $admin->givePermissionTo($delete_property);
    }
}

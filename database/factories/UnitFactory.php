<?php

use Faker\Generator as Faker;

$factory->define(App\Unit::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid(),
        'unit_title' => $faker->text(20),
        'unit_description' => $faker->text(300),
        'price_fee' => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 1000),
        'security_deposit' => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 1000),
        'listed' => $faker->boolean(),
        'property_id' => $faker->numberBetween($min = 1, $max = 10)
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Property::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid(),
        'property_title' => $faker->text(20),
        'property_description' => $faker->text(300),
        'street' => $faker->streetAddress(),
        'city' => $faker->city(),
        'state' => $faker->state(),
        'country' => $faker->country(),
        'user_id' => $faker->numberBetween($min = 1, $max = 1)
    ];
});

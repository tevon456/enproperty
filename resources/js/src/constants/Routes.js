//PUBLIC
export const HOME = "/";
export const LISTING = "/listing";
export const ID = "/:id";
export const TERMS = "/terms";

//API
export const API = "/api";
export const PROPERTY = "/property";
export const UNIT = "/units";

//AUTH
export const TOKEN = "/:token";
export const AUTH = "/auth";
export const V = ["/v1.01", "/v1.02"];
export const VERIFY = "/verify";
export const ACTIVATE = "/activate";
export const REACTIVATE = "/resend-activation";
export const LOGIN = "/login";
export const LOGOUT = "/logout";
export const LIKE = "/like";
export const SIGN_UP = "/signup";
export const USER = "/user";
export const UPDATE = "/update";
export const RESET = "/reset";
export const FIND = "/find";
export const CREATE = "/create";
export const PASSWORD = "/password";
export const PASSWORD_RESET = "/password-reset";

//PROTECTED
export const OVERVIEW = "/overview";
export const SAVED = "/saved";
export const ACCOUNT = "/account";
export const SETTINGS = "/settings";

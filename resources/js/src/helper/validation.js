import * as Yup from "yup";

const copy = {
    required: "Required",
    noSpace: "No space",
    email: "Invalid Email",
    tooShort: "Too Short",
    tooLong: "Too Long",
    minVal: "Value must be 0 or greater",
    positive: "Must be positive"
};
const email = Yup.string()
    .trim(copy.required)
    .strict(true)
    .email(copy.email)
    .required(copy.required);

const name = Yup.string()
    .min(2, copy.tooShort)
    .max(50, copy.tooLong)
    .trim(copy.noSpace)
    .strict(true)
    .required(copy.required);

const password = Yup.string()
    .trim(copy.noSpace)
    .strict(true)
    .min(8, "at least 8 characters")
    .max(24, copy.tooLong)
    .required(copy.required);

const title = Yup.string()
    .min(8, copy.tooShort)
    .max(60, copy.tooLong)
    .required(copy.required);

const city = Yup.string()
    .min(5, copy.tooShort)
    .max(60, copy.tooLong);

const cell = Yup.string()
    .required()
    .min(17, "Not a valid contact number");

const code = Yup.number()
    .required()
    .min(100000, "Must be 6 digits")
    .max(999999, "Too many digits");

const fee = Yup.string()
    .required()
    .min(2, "too little");

const deposit = Yup.string();

const role = Yup.string().required();

const listed = Yup.bool().required(copy.required);

const uuid = Yup.string().required(copy.required);
// .trim(copy.noSpace);

const description = Yup.string().max(500, copy.tooLong);

//SCHEMAS
export const SignupSchema = Yup.object().shape({
    firstName: name,
    lastName: name,
    email: email,
    password: password,
    role: role
});

export const LoginSchema = Yup.object().shape({
    email: email,
    password: password
});

export const PasswordResetSchema = Yup.object().shape({
    email: email
});

export const UpdatePasswordSchema = Yup.object().shape({
    password: password
});

export const UserNameSchema = Yup.object().shape({
    firstName: name,
    lastName: name
});

export const UserEmailSchema = Yup.object().shape({
    email: email
});

export const UserCellSchema = Yup.object().shape({
    contact_number: cell
});

export const VerificationCodeSchema = Yup.object().shape({
    code: code,
    contact_number: Yup.string()
        .required()
        .min(11, "Not a valid contact number")
});

export const PropertySchema = Yup.object().shape({
    property_title: title,
    property_description: description,
    street: title,
    city: city,
    state: title
});

export const UnitSchema = Yup.object().shape({
    unit_title: title,
    unit_description: description,
    price_fee: fee,
    security_deposit: deposit,
    listed: listed,
    property_id: uuid
});

import {
    ACTIVATE,
    API,
    AUTH,
    CREATE,
    FIND,
    LIKE,
    LOGIN,
    LOGOUT,
    PASSWORD,
    PROPERTY,
    REACTIVATE,
    RESET,
    SIGN_UP,
    UNIT,
    USER,
    V,
    VERIFY
} from "../constants/Routes";
import { getToken, isUserAuthenticated } from "./authHelper";

import { UPDATE } from "./../constants/Routes";
import axios from "axios";

axios.defaults.baseURL = location.origin + API;
axios.defaults.headers.common = {
    "Content-Type": "application/json",
    "X-Requested-With": "XMLHttpRequest",
    Authorization: `Bearer ${getToken()}`
};

/**
 * Headers for sending files to an endpoint
 */
const fileHeader = {
    "content-type":
        "multipart/form-data; charset=utf-8; boundary=" +
        Math.random()
            .toString()
            .substr(2)
};

/**
 * Send post request with user signup information.
 *
 * @param {object} value
 */
export async function signUp(value) {
    let url = AUTH + SIGN_UP;
    const res = await axios.post(url, value);
    return res;
}

/**
 * Activate account with token and check if valid.
 *
 * @param {string} token
 */
export async function activateAccount(token) {
    let url = AUTH + SIGN_UP + ACTIVATE + `/${token}`;
    const res = await axios.get(url);
    return res;
}

/**
 * Resend email actvation link to user
 *
 * @param {string} email
 */
export async function resendActivation(email) {
    let url = AUTH + SIGN_UP + REACTIVATE;
    const res = await axios.get(url, email);
    return res;
}

/**
 * Login user and return token on success.
 *
 * @param {object} value
 */
export async function login(value) {
    let url = AUTH + LOGIN;
    const res = await axios.post(url, value);
    return res;
}

/**
 * logout user using token.
 */
export async function logout() {
    let url = AUTH + LOGOUT;
    const res = await axios.get(url);
    return res;
}

/**
 * Sends password reset request to email provided as param
 *
 * @param {string} email
 */
export async function forgotPassword(email) {
    let url = PASSWORD + CREATE;
    const res = await axios({ method: "post", url: url, data: email });
    return res;
}

/**
 * Checks if the password token provided is valid
 *
 * @param {string} token
 */
export async function findPassword(token) {
    let url = PASSWORD + FIND + `/${token}`;
    const res = await axios({ method: "get", url: url, data: token });
    return res;
}

/**
 * Updates password with new password, requires auth token param to be set.
 *
 * @param {object} values
 */
export async function updatePassword(values) {
    let url = PASSWORD + RESET;
    const res = await axios({ method: "post", url: url, data: values });
    return res;
}

/**
 * Fetch user's data using token.
 *
 */
export async function getUser() {
    let url = V[0] + AUTH + USER;
    const res = await axios.get(url, { withCredentials: true });
    return res;
}

/**
 * Update user personal data
 * @param {object} data
 */
export async function updateUser(data) {
    let url = V[0] + AUTH + USER + UPDATE;
    const res = await axios({ method: "patch", url: url, data: data });
    return res;
}

/**
 * Get paginated properties or a single property using id param.
 *
 * @param {string} id
 */
export async function getProperties(id) {
    let url = null;
    {
        isUserAuthenticated() ? (url = AUTH + PROPERTY) : (url = PROPERTY);
    }
    {
        id === undefined ? (url = url) : (url = url + `/${id}`);
    }
    const res = await axios({ method: "get", url: url });
    return res;
}

/**
 * Get properties owned by user.
 *
 * @param {string} id
 */
export async function getUserProperties() {
    let url = V[0] + AUTH + USER + PROPERTY;
    const res = await axios.get(url);
    return res;
}

/**
 * Get units owned by user.
 *
 */
export async function getUserUnits() {
    let url = V[0] + AUTH + USER + UNIT;
    const res = await axios.get(url);
    return res;
}

/**
 * Get units saved by user.
 *
 */
export async function getUserSaved() {
    let url = V[0] + AUTH + USER + LIKE;
    const res = await axios({ method: "get", url: url });
    return res;
}

/**
 * Get paginated properties.
 *
 * @param {string} id
 */
export async function getListing(page) {
    let url = null;
    {
        isUserAuthenticated()
            ? (url = V[0] + AUTH + UNIT + `?page=${page ? page : 1}`)
            : (url = V[0] + UNIT + `?page=${page ? page : 1}`);
    }
    const res = await axios.get(url);
    return res;
}

/**
 * Get a single property using id param.
 *
 * @param {string} id
 */
export async function getUnit(id) {
    let url = null;
    {
        isUserAuthenticated()
            ? (url = V[0] + AUTH + UNIT + `/${id}`)
            : (url = V[0] + UNIT + `/${id}`);
    }
    const res = await axios.get(url);
    return res;
}

/**
 * Create a new property
 *
 * @param {object} values
 */
export async function createProperty(values) {
    let url = V[0] + AUTH + PROPERTY;
    const res = await axios({ method: "post", url: url, data: values });
    return res;
}

/**
 * Edit owned property using the property's id
 *
 * @param {object} data
 *
 */
export async function updateProperty(data) {
    let url = V[0] + AUTH + PROPERTY + UPDATE;
    const res = await axios({ method: "patch", url: url, data: data });
    return res;
}

/**
 * Delete Property using id
 *
 *@param {number} id
 */
export async function deleteProperty(id) {
    let url = V[0] + AUTH + PROPERTY + "/" + id;
    const res = await axios({ method: "delete", url: url });
    return res;
}

/**
 * Like a unit using it's uuid
 *
 * @param {object} values
 */
export async function likeUnit(uuid) {
    let url = V[0] + AUTH + UNIT + LIKE + `/${uuid}`;
    const res = await axios({ method: "get", url: url });
    return res;
}

/**
 * Create a new unit
 *
 * @param {object} values
 */
export async function createUnit(values) {
    let url = V[0] + AUTH + UNIT;
    const res = await axios({ method: "post", url: url, data: values });
    return res;
}

/**
 * Update unit data
 * @param {object} data
 */
export async function updateUnit(data) {
    let url = V[0] + AUTH + UNIT + UPDATE;
    const res = await axios({ method: "patch", url: url, data: data });
    return res;
}

/**
 * Delete unit
 * @param {object} data
 */
export async function deleteUnit(id) {
    let url = V[0] + AUTH + UNIT + `/${id}`;
    const res = await axios({ method: "delete", url: url });
    return res;
}

/**
 * Upload unit images
 *
 * @param {object} values
 */
export async function uploadUnitImage(values) {
    let url = V[0] + AUTH + UNIT + "/image";
    const res = await axios({
        method: "post",
        url: url,
        data: values,
        headers: fileHeader
    });
    return res;
}

/**
 * Delete a unit's image
 *
 * @param {string} values
 */
export async function deleteUnitImage(id) {
    let url = V[0] + AUTH + UNIT + `/image`;
    const res = await axios({
        method: "delete",
        url: url,
        data: { id: id }
    });
    return res;
}

/**
 * Upload unit images
 *
 * @param {object} values
 */
export async function uploadProfileImage(values) {
    let url = V[0] + AUTH + USER + "/avatar";
    const res = await axios({
        method: "post",
        url: url,
        data: values,
        headers: fileHeader
    });
    return res;
}

/**
 * Request a verification be sent for a phone number
 * @param {object} data
 */
export async function requestVerification(data) {
    let url = V[0] + AUTH + USER + VERIFY + `/request`;
    const res = await axios({ method: "post", url: url, data: data });
    return res;
}

/**
 * Check verification code
 * @param {object} data
 */
export async function verifyCode(data) {
    let url = V[0] + AUTH + USER + VERIFY + `/code`;
    const res = await axios({ method: "post", url: url, data: data });
    return res;
}

export async function test() {
    let url = `login/facebook`;
    const res = await axios({ method: "get", url: url });
    return res;
}

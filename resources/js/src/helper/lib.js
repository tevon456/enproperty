import ReactGA from "react-ga";

export function convertToSlug(Text) {
    return Text.toLowerCase()
        .replace(/[^\w ]+/g, "")
        .replace(/ +/g, "-");
}

export function crispChat(website_id) {
    window.$crisp = [];
    window.CRISP_WEBSITE_ID = website_id;

    (function() {
        var d = document;
        var s = d.createElement("script");

        s.src = "https://client.crisp.chat/l.js";
        s.async = 1;
        d.getElementsByTagName("head")[0].appendChild(s);
    })();
    $crisp.push(["safe", true]);
    window.CRISP_READY_TRIGGER = function() {
        moveCrisp();
    };
}

export function googleTracking(tracking_id) {
    ReactGA.initialize(tracking_id, {
        debug: false
    });
    ReactGA.pageview(window.location.pathname);
}

function moveCrisp() {
    let bubble = document.querySelector("#crisp-chatbox > div > a");
    bubble.setAttribute("style", "transform: translateY(-48px) scale(0.9);");
}

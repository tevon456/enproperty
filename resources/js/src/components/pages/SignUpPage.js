import Head from "../elements/Head";
import Page from "../elements/Page";
import React from "react";
import { SignUpForm } from "./../ui/inputs/Form";

function SignUpPage() {
    return (
        <Page padding={true}>
            <Head title="Sign Up" />
            <div className="grid-center margin-top--md">
                <SignUpForm />
            </div>
        </Page>
    );
}
export default SignUpPage;

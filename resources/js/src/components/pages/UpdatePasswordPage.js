import Page from "../elements/Page";
import React from "react";
import { UpdatePasswordForm } from "./../ui/inputs/Form";

function UpdatePasswordPage(props) {
    return (
        <Page padding={true}>
            <Head title="New password" />
            <div className="grid-center margin-top--md">
                <UpdatePasswordForm match={props.match} />
            </div>
        </Page>
    );
}
export default UpdatePasswordPage;

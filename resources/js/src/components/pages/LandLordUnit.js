import { FileUploadForm, UnitForm } from "../ui/inputs/Form";
import { FlexItem, FlexView } from "./../elements/FlexView";
import { LISTING, OVERVIEW } from "../../constants/Routes";
import React, { Component } from "react";
import {
    deleteUnit,
    deleteUnitImage,
    updateUnit
} from "../../helper/apiHelper";

import Badge from "./../ui/surfaces/Badge";
import { BaseCard } from "../ui/surfaces/Card";
import Container from "../ui/content/Container";
import DropdownButton from "../ui/inputs/DropdownButton";
import EmptyState from "../ui/content/EmptyState";
import Head from "../elements/Head";
import { IconTrash } from "../ui/content/Icon";
import { Line } from "../ui/content/Line";
import { Link } from "react-router-dom";
import Modal from "react-modal";
import { ModalHeader } from "../ui/surfaces/Modal";
import { Note } from "./../ui/content/Note";
import { OverviewContext } from "./../elements/OverviewRoute";
import Page from "../elements/Page";
import SkipLanding from "../elements/SkipLanding";
import Space from "../ui/content/WhiteSpace";
import { convertToSlug } from "../../helper/lib";
import notification from "../../helper/notificationHelper";

class Unit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            modalTitle: null,
            content: null
        };
        this.setState = this.setState.bind(this);
        this.openModal = this.openModal.bind(this);
        this.delete = this.delete.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.toggleUnitListed = this.toggleUnitListed.bind(this);
    }

    openModal(id, data) {
        this.renderContent(id, data);
    }

    closeModal() {
        this.setState({ modalIsOpen: false });
        this.props.refresh();
    }

    delete(id) {
        deleteUnit(id)
            .then(() => {
                this.closeModal();
                notification.success("Unit deleted");
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }

    deleteImage(id) {
        deleteUnitImage(id)
            .then(res => {
                this.closeModal();
                notification.success(res.data.message);
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }

    toggleUnitListed(data) {
        let toggle = Object.assign({}, data);
        if (data.listed == 1) {
            toggle.listed = false;
        } else {
            toggle.listed = true;
        }
        toggle.unit_id = toggle.uuid;
        delete toggle.uuid;
        updateUnit(toggle).then(res => {
            notification.info(
                toggle.listed == true
                    ? "Your unit is now available"
                    : "Unit is now hidden from public"
            );
            this.props.refresh();
        });
    }

    carbonParse(date) {
        let readable = Date.parse(date);
        var newDate = new Date();
        newDate.setTime(readable);
        let dateString = newDate.toLocaleString();
        return dateString;
    }

    renderContent(key, data) {
        switch (key) {
            case 1:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Edit",
                    content: (
                        <OverviewContext.Consumer>
                            {overview => (
                                <div className="grid-center">
                                    <div className="margin-top--lg margin-bottom--lg">
                                        <UnitForm
                                            closeModal={this.closeModal}
                                            data={this.props.data}
                                            update={data}
                                        />
                                    </div>
                                </div>
                            )}
                        </OverviewContext.Consumer>
                    )
                });
                break;
            case 2:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Delete",
                    modalBlur: false,
                    content: (
                        <div className="grid-center">
                            <div style={{ padding: "20px" }}>
                                <p>
                                    Are you sure you want to delete this unit?
                                    <br />
                                    You will loose:
                                </p>
                                <ul>
                                    <li>This Unit</li>
                                    <li>It's {data.images.length} Images</li>
                                    <li>Any other related data</li>
                                </ul>
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-around"
                                    }}
                                >
                                    <button
                                        className="btn btn--bordered btn--full-width"
                                        onClick={this.closeModal}
                                    >
                                        Cancel
                                    </button>
                                    <div
                                        style={{ margin: "var(--space-xs)" }}
                                    />
                                    <button
                                        onClick={() => {
                                            this.delete(data.uuid);
                                        }}
                                        className="btn btn--danger btn--full-width"
                                    >
                                        Yes, Delete
                                    </button>
                                </div>
                            </div>
                        </div>
                    )
                });
                break;
            case 3:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Details",
                    content: (
                        <div className="grid-center">
                            <div className="margin-top--lg margin-bottom--lg">
                                <div className="text--rg text-grey margin-top--sm margin-bottom--sm">
                                    <b>Name of unit: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        {data.unit_title}
                                    </p>
                                </div>
                                <div className="text--rg margin-top--sm margin-bottom--sm">
                                    <b>Unit listed: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        {data.listed == 1 ? "Yes" : "No"}
                                    </p>
                                </div>
                                <div
                                    style={{ width: "300px" }}
                                    className="text--rg text-grey margin-top--sm margin-bottom--sm"
                                >
                                    <b>Description: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        {data.unit_description
                                            ? data.unit_description
                                            : "none"}
                                    </p>
                                </div>
                                <div className="text--rg margin-top--sm margin-bottom--sm">
                                    <b>Rent per month: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        ${data.price_fee}
                                    </p>
                                </div>
                                <div className="text--rg margin-top--sm margin-bottom--sm">
                                    <b>Security deposit: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        ${data.security_deposit}
                                    </p>
                                </div>
                                <div className="text--rg margin-top--sm margin-bottom--sm">
                                    <b>Address: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        {data.parent_property.street},{" "}
                                        {data.parent_property.city},{" "}
                                        {data.parent_property.state}
                                    </p>
                                </div>
                                <div className="text--rg margin-top--sm margin-bottom--sm">
                                    <b>Unit belongs to property: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        {data.parent_property.property_title}
                                    </p>
                                </div>
                                <div className="text--rg margin-top--sm margin-bottom--sm">
                                    <b>Unit added on: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        {this.carbonParse(data.created_at)}
                                    </p>
                                </div>
                                <div className="text--rg margin-top--sm margin-bottom--sm">
                                    <b>Last edit: </b>
                                    <p className="text--blue margin-top--none margin-bottom--none">
                                        {this.carbonParse(data.updated_at)}
                                    </p>
                                </div>
                            </div>
                        </div>
                    )
                });
                break;
            case 4:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Manage Images",
                    content: (
                        <div className="margin-top--none margin-bottom--lg">
                            <div
                                className="grid-center"
                                style={{
                                    padding: "var(--space-sm) var(--space-sm)"
                                }}
                            >
                                <FileUploadForm
                                    close={this.closeModal}
                                    data={data}
                                />
                            </div>

                            <Line />

                            <FlexView>
                                {data.images.length > 0 ? (
                                    data.images.map(i => (
                                        <FlexItem key={i.uuid}>
                                            <BaseCard src={i.url}>
                                                <button
                                                    onClick={() =>
                                                        this.deleteImage(i.uuid)
                                                    }
                                                    style={{
                                                        marginTop: "-4px"
                                                    }}
                                                    className="btn btn--secondary btn--full-width "
                                                >
                                                    <IconTrash />
                                                    <Space amount={1} />
                                                    remove
                                                </button>
                                            </BaseCard>
                                        </FlexItem>
                                    ))
                                ) : (
                                    <Note>
                                        No images were uploaded for this unit
                                    </Note>
                                )}
                            </FlexView>
                        </div>
                    )
                });
                break;
            default:
                this.setState({
                    modalIsOpen: true,
                    modalTitle: "Modal",
                    content: (
                        <b className="margin-top--lg margin-bottom--lg">
                            Pass an id to the openModal()
                        </b>
                    )
                });
                break;
        }
    }

    render() {
        return (
            <OverviewContext.Consumer>
                {context => (
                    <Page>
                        <Head title="My Units" />
                        <span
                            aria-label="all the units you own show up here, you can edit, view and delete units from this tab"
                            data-balloon-pos="right"
                            data-balloon-length="medium"
                        >
                            <Badge>?</Badge>
                        </span>
                        <SkipLanding name="content" text="content" />
                        <div className="margin-bottom--xxs" />
                        <Container>
                            {context.state.userUnits.length > 0 ? (
                                <section className="">
                                    <FlexView justify="center">
                                        {context.state.userUnits.map(d => (
                                            <FlexItem key={d.uuid}>
                                                <BaseCard
                                                    width="270"
                                                    overflow="intial"
                                                    src={
                                                        d.images[0]
                                                            ? d.images[0].url
                                                            : null
                                                    }
                                                >
                                                    <b className="truncate-wrapper">
                                                        <p
                                                            style={{
                                                                marginBottom:
                                                                    "-0.5em"
                                                            }}
                                                            className="truncate margin-top--none"
                                                        >
                                                            <span
                                                                style={{
                                                                    padding:
                                                                        "4px",
                                                                    display:
                                                                        "inline-block",
                                                                    cursor:
                                                                        "default",
                                                                    borderRadius:
                                                                        "3px",
                                                                    zIndex: 1,
                                                                    backgroundImage: `linear-gradient(${
                                                                        d.listed ==
                                                                        1
                                                                            ? "#19fd63, var(--green)"
                                                                            : "gray,#cac5c5"
                                                                    })`,
                                                                    width:
                                                                        "8px",
                                                                    height:
                                                                        "8px"
                                                                }}
                                                            />{" "}
                                                            {d.unit_title}
                                                        </p>
                                                    </b>
                                                    <p>
                                                        {
                                                            d.parent_property
                                                                .street
                                                        }
                                                        ,{" "}
                                                        {d.parent_property.city}
                                                    </p>

                                                    <div className="margin-top--sm" />
                                                    <DropdownButton
                                                        text="Edit"
                                                        className="btn btn--secondary btn--full-width "
                                                        x="-72%"
                                                        color="white"
                                                        dropId={d.uuid}
                                                        y="26px"
                                                        width="170px"
                                                        onClick={() =>
                                                            this.openModal(1, d)
                                                        }
                                                    >
                                                        {d.listed == 1 ? (
                                                            <li tabIndex="0">
                                                                <Link
                                                                    disabled
                                                                    rel="noopener noreferrer"
                                                                    target="_blank"
                                                                    to={
                                                                        LISTING +
                                                                        `/${convertToSlug(
                                                                            d.unit_title
                                                                        )}/${
                                                                            d.uuid
                                                                        }`
                                                                    }
                                                                >
                                                                    View
                                                                </Link>{" "}
                                                            </li>
                                                        ) : null}
                                                        <li
                                                            tabIndex="0"
                                                            onClick={() =>
                                                                this.openModal(
                                                                    3,
                                                                    d
                                                                )
                                                            }
                                                        >
                                                            Details
                                                        </li>
                                                        <li
                                                            tabIndex="0"
                                                            onClick={() =>
                                                                this.toggleUnitListed(
                                                                    d
                                                                )
                                                            }
                                                        >
                                                            {d.listed == 1
                                                                ? "Delist"
                                                                : "List"}
                                                        </li>
                                                        <li
                                                            tabIndex="0"
                                                            onClick={() =>
                                                                this.openModal(
                                                                    4,
                                                                    d
                                                                )
                                                            }
                                                        >
                                                            Manage Images
                                                        </li>
                                                        <li
                                                            tabIndex="0"
                                                            onClick={() =>
                                                                this.openModal(
                                                                    2,
                                                                    d
                                                                )
                                                            }
                                                        >
                                                            <span className="text--red">
                                                                Delete
                                                            </span>
                                                        </li>
                                                    </DropdownButton>
                                                </BaseCard>
                                            </FlexItem>
                                        ))}
                                    </FlexView>
                                </section>
                            ) : (
                                <section className="animated fadeIn delay-3s grid-center">
                                    <EmptyState
                                        maxWidth="300px"
                                        title="No Units as yet"
                                        message="All your units will be listed here, click
                            below to go to the overview page where you can add a unit."
                                        link={OVERVIEW}
                                        linkText="Go to overview"
                                    />
                                </section>
                            )}
                        </Container>
                        <div className="margin-bottom--md" />
                        <Modal
                            isOpen={this.state.modalIsOpen}
                            onRequestClose={this.closeModal}
                            blur={true}
                            contentLabel="Modal"
                            className="modal-content"
                            overlayClassName="modal-overlay"
                        >
                            <ModalHeader
                                modalTitle={this.state.modalTitle}
                                closeModal={this.closeModal}
                            />
                            {this.state.content}
                            <div className="margin-bottom--lg" />
                        </Modal>
                    </Page>
                )}
            </OverviewContext.Consumer>
        );
    }
}
export default Unit;

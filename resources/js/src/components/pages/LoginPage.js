import Head from "../elements/Head";
import { LoginForm } from "./../ui/inputs/Form";
import Page from "../elements/Page";
import React from "react";
import SkipLanding from "../elements/SkipLanding";

function LoginPage() {
    return (
        <Page padding={true}>
            <Head title="Login" />
            <div className="grid-center margin-top--md">
                <SkipLanding name="content" />
                <LoginForm />
            </div>
        </Page>
    );
}
export default LoginPage;

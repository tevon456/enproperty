import { LOGIN, PASSWORD_RESET, UPDATE } from "./../../constants/Routes";
import React, { Component } from "react";
import { activateAccount, findPassword } from "../../helper/apiHelper";

import Container from "../ui/content/Container";
import CustomLink from "../ui/navigation/CustomLink";
import Head from "../elements/Head";
import Page from "../elements/Page";
import { set } from "idb-keyval";

class Token extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content: {},
            option: null,
            isLoading: true
        };
        this.setState = this.setState.bind(this);
        this.TokenPageContent = this.TokenPageContent.bind(this);
    }

    componentDidMount() {
        this.TokenPageContent();
    }

    TokenPageContent() {
        //new object for content of page
        let content = {};
        //Get the first pathname to determine
        const path = this.props.match.path.split("/")[1];
        //Get token form param
        const token = this.props.match.params.token;
        if (path === "activate") {
            content.title = "Account activation";
            activateAccount(token)
                .then(res => {
                    content.message = res.data.message;
                    this.setState({
                        content: content,
                        option: (
                            <CustomLink to={LOGIN}>
                                Click here to login
                            </CustomLink>
                        ),
                        isLoading: false
                    });
                })
                .catch(error => {
                    if (error.response) {
                        content.message = error.response.data.message;
                        this.setState({
                            content: content,
                            isLoading: false
                        });
                    }
                });
        } else {
            content.title = "Password reset";
            findPassword(token)
                .then(res => {
                    content.message = "Password token valid";
                    console.log(res.data);
                    set("reset", res.data);
                    this.setState({
                        content: content,
                        option: (
                            <CustomLink
                                to={
                                    PASSWORD_RESET +
                                    UPDATE +
                                    `/${res.data.token}`
                                }
                            >
                                Click here to continue
                            </CustomLink>
                        ),
                        isLoading: false
                    });
                })
                .catch(error => {
                    if (error.response) {
                        content.message = error.response.data.message;
                        this.setState({
                            content: content,
                            option: (
                                <CustomLink to={PASSWORD_RESET}>
                                    Try resetting again
                                </CustomLink>
                            ),
                            isLoading: false
                        });
                    }
                });
        }
    }

    render() {
        let d = this.state;
        return (
            <Page padding={true}>
                <Head title="Token" />
                {d.isLoading ? (
                    <h3>Loading...</h3>
                ) : (
                    <React.Fragment>
                        <h1>{d.content.title}</h1>
                        <Container>
                            <p>{d.content.message}</p>
                            {d.option}
                        </Container>
                    </React.Fragment>
                )}
            </Page>
        );
    }
}

export default Token;

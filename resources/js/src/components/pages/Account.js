import {
    ProfileImageUpload,
    UpdateCellForm,
    UpdateNameForm,
    VerificationCodeForm
} from "../ui/inputs/Form";
import React, { Component } from "react";

import { AppContext } from "./../../App";
import { Avatar } from "../ui/content/Avatar";
import Badge from "./../ui/surfaces/Badge";
import Can from "../elements/Can";
import Container from "../ui/content/Container";
// import { Doughnut } from "react-chartjs-2";
import ErrorBoundary from "../ui/content/Error";
import Footer from "../ui/content/Footer";
import Head from "./../elements/Head";
import { IconEdit } from "../ui/content/Icon";
import { Line } from "../ui/content/Line";
import Note from "../ui/content/Note";
import { OverideForm } from "./../ui/inputs/Form";
import Page from "../elements/Page";
import SkipLanding from "../elements/SkipLanding";
import Space from "../ui/content/WhiteSpace";
import Theme from "../../helper/themeHelper";
import Toggle from "../ui/inputs/Toggle";
import { deauthenticateUser } from "../../helper/authHelper";

class Account extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEditingName: false,
            isEditingEmail: false,
            isEditingAvatar: false,
            isEditingCell: false
        };
        this.setState = this.setState.bind(this);
        this.ToggleAvatar = this.ToggleAvatar.bind(this);
        this.ToggleName = this.ToggleName.bind(this);
        this.ToggleEmail = this.ToggleEmail.bind(this);
    }

    ToggleAvatar(state) {
        if (state == true) {
            this.setState({
                isEditingAvatar: false
            });
        } else {
            this.setState({
                isEditingAvatar: true
            });
        }
    }

    ToggleCell(state) {
        if (state == true) {
            this.setState({
                isEditingCell: false
            });
        } else {
            this.setState({
                isEditingCell: true
            });
        }
    }

    ToggleName(state) {
        if (state == true) {
            this.setState({
                isEditingName: false
            });
        } else {
            this.setState({
                isEditingName: true
            });
        }
    }

    ToggleEmail(state) {
        if (state == true) {
            this.setState({
                isEditingEmail: false
            });
        } else {
            this.setState({
                isEditingEmail: true
            });
        }
    }

    logUserOut() {
        deauthenticateUser();
        setTimeout(() => {
            location.reload();
        }, 2000);
    }

    render() {
        return (
            <ErrorBoundary>
                <AppContext.Consumer>
                    {context => (
                        <React.Fragment>
                            <Page padding={true}>
                                <Head title="Account" />
                                <SkipLanding name="content" />
                                <h1>Account</h1>
                                <div className="btns">
                                    <button
                                        onClick={this.logUserOut}
                                        className="btn btn--secondary btn--sm text--md"
                                    >
                                        Logout
                                    </button>
                                </div>
                                <Container>
                                    <EditSectionAction
                                        title="Profile picture"
                                        state={this.state.isEditingAvatar}
                                        action={() =>
                                            this.ToggleAvatar(
                                                this.state.isEditingAvatar
                                            )
                                        }
                                    />
                                    {this.avatarSection(context)}

                                    <br />
                                    <Line className="margin-top--md" />

                                    <EditSectionAction
                                        title="Name"
                                        state={this.state.isEditingName}
                                        action={() =>
                                            this.ToggleName(
                                                this.state.isEditingName
                                            )
                                        }
                                    />
                                    {this.nameSection(context)}
                                    <Line className="margin-top--md" />

                                    <EditSectionAction
                                        title="Email address"
                                        state={this.state.isEditingEmail}
                                        disabled={true}
                                        action={() =>
                                            this.ToggleEmail(
                                                this.state.isEditingEmail
                                            )
                                        }
                                    />
                                    {this.emailSection(context)}
                                    <Line className="margin-top--md" />

                                    <Can i={["create listing"]}>
                                        <EditSectionAction
                                            title="Phone number"
                                            state={this.state.isEditingCell}
                                            action={() =>
                                                this.ToggleCell(
                                                    this.state.isEditingCell
                                                )
                                            }
                                        />
                                        {
                                            <div>
                                                {this.state.isEditingCell ? (
                                                    <React.Fragment>
                                                        <OverideForm>
                                                            <UpdateCellForm
                                                                data={
                                                                    context
                                                                        .state
                                                                        .user
                                                                }
                                                            />
                                                            <VerificationCodeForm />
                                                        </OverideForm>
                                                        <Note>
                                                            <b>Note</b>
                                                            <br />
                                                            When adding or
                                                            updating your number
                                                            a verification code
                                                            will be sent to
                                                            ensure that you own
                                                            the number.
                                                        </Note>
                                                    </React.Fragment>
                                                ) : (
                                                    <React.Fragment>
                                                        <p>
                                                            {
                                                                context.state
                                                                    .user.cell
                                                            }
                                                            {context.state.user
                                                                .cell ? (
                                                                <span
                                                                    aria-label="number verified"
                                                                    data-balloon-pos="up"
                                                                >
                                                                    <Badge background="var(--green)">
                                                                        verified
                                                                    </Badge>
                                                                </span>
                                                            ) : (
                                                                <span
                                                                    aria-label="number not added"
                                                                    data-balloon-pos="up"
                                                                >
                                                                    <Badge background="red">
                                                                        no
                                                                        number
                                                                    </Badge>
                                                                </span>
                                                            )}
                                                        </p>
                                                    </React.Fragment>
                                                )}
                                            </div>
                                        }
                                    </Can>
                                </Container>

                                <h1>Settings</h1>
                                <Container>
                                    <div
                                        style={{
                                            display: "flex",
                                            justifyContent: "space-between"
                                        }}
                                    >
                                        <b className="margin-top--sm margin-bottom--md">
                                            Dark mode{" "}
                                            <span
                                                aria-label="reduces eye strain when enabled at night"
                                                data-balloon-pos="up"
                                            >
                                                <Badge>?</Badge>
                                            </span>
                                        </b>
                                        <Toggle
                                            on={Theme.get()}
                                            switchOn={() => Theme.set()}
                                            switchOff={() => Theme.set()}
                                        />
                                    </div>
                                </Container>
                            </Page>
                            <Footer />
                        </React.Fragment>
                    )}
                </AppContext.Consumer>
            </ErrorBoundary>
        );
    }

    emailSection(context) {
        return (
            <div className="margin-bottom--lg">
                {this.state.isEditingEmail ? (
                    <OverideForm>
                        {/* <UpdateEmailForm email={context.state.user.email} /> */}
                    </OverideForm>
                ) : (
                    <React.Fragment>
                        <p>{context.state.user.email}</p>
                    </React.Fragment>
                )}
            </div>
        );
    }

    nameSection(context) {
        return (
            <div className="margin-bottom--lg">
                {this.state.isEditingName ? (
                    <OverideForm>
                        <UpdateNameForm data={context.state.user} />
                    </OverideForm>
                ) : (
                    <React.Fragment>
                        <p>
                            {context.state.user.first_name}{" "}
                            {context.state.user.last_name}
                        </p>
                    </React.Fragment>
                )}
            </div>
        );
    }

    avatarSection(context) {
        return (
            <div className="margin-bottom--sm">
                {this.state.isEditingAvatar ? (
                    <div className="grid-center animated fadeIn">
                        <ProfileImageUpload />
                    </div>
                ) : (
                    <div className="grid-center animated fadeIn">
                        <div className="margin-bottom--lg">
                            <Avatar
                                style={{
                                    display: "grid"
                                }}
                                first_name={context.state.user.first_name}
                                last_name={context.state.user.last_name}
                                size="huge"
                                src={context.state.user.avatar}
                            />
                        </div>
                    </div>
                )}
            </div>
        );
    }
}
export default Account;

function EditSectionAction(props) {
    return (
        <div
            style={{
                display: "flex",
                justifyContent: "space-between"
            }}
            className="margin-bottom--md"
        >
            <b style={{ lineHeight: 2 }}>{props.title}</b>
            {props.state ? (
                <button
                    onClick={props.action}
                    className="btn btn--bordered btn--sm text--md"
                >
                    Cancel
                </button>
            ) : (
                <button
                    onClick={props.action}
                    disabled={props.disabled}
                    className="btn btn--bordered btn--sm text--md"
                >
                    <IconEdit /> <Space amount={1} /> Edit
                </button>
            )}
        </div>
    );
}

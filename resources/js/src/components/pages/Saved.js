import { FlexItem, FlexView } from "../elements/FlexView";
import React, { useEffect, useState } from "react";

import { BaseCard } from "../ui/surfaces/Card";
import EmptyState from "../ui/content/EmptyState";
import Head from "../elements/Head";
import { LISTING } from "../../constants/Routes";
import { Link } from "react-router-dom";
import Page from "../elements/Page";
import SkipLanding from "../elements/SkipLanding";
import { convertToSlug } from "../../helper/lib";
import { getUserSaved } from "../../helper/apiHelper";

function Saved() {
    //create initial state of listing
    const [saves, setSaves] = useState([]);
    //Get data from network/api
    function fromNetwork() {
        getUserSaved()
            .then(res => {
                setSaves(res.data);
            })
            .catch(error => {
                if (error.response) {
                    console.log(error.response);
                }
            });
    }
    useEffect(() => {
        fromNetwork();
    }, []);
    return (
        <Page padding={false}>
            <Head title="Saved units" />
            <SkipLanding name="content" />
            <h1 className="text--center">Saved</h1>
            {saves.length > 0 ? (
                <section className="animated fadeIn">
                    <FlexView>
                        {saves.map(d => (
                            <List key={d.id} data={d} />
                        ))}
                    </FlexView>
                </section>
            ) : (
                <section className="animated fadeIn delay-3s grid-center">
                    <EmptyState
                        maxWidth="300px"
                        title="Nothing saved as yet"
                        message="Save units your interested in for
                            later by clicking the button next to 'contact' on a listing details page."
                        link={LISTING}
                        linkText="Listing page"
                    />
                </section>
            )}
            <div className="margin-bottom--lg" />
        </Page>
    );
}
function List(props) {
    let d = props.data;
    return (
        <FlexItem>
            <BaseCard width={300}>
                <b className="truncate-wrapper">
                    <p
                        style={{
                            marginBottom: "-0.5em"
                        }}
                        className="truncate margin-top--none"
                    >
                        {d.unit_title}
                    </p>
                </b>
                <div className="margin-bottom--sm margin-top--md">
                    <b>
                        <span>${Math.round(d.price_fee * 100) / 100}</span>

                        <span className="text--grey">{" | "}per month</span>
                    </b>
                </div>
                <div className="btns">
                    <Link
                        tabIndex={0}
                        to={
                            LISTING +
                            `/${convertToSlug(d.unit_title)}/${d.uuid}`
                        }
                        className="btn btn--secondary btn--full-width "
                    >
                        view
                    </Link>
                </div>
            </BaseCard>
        </FlexItem>
    );
}

export default Saved;

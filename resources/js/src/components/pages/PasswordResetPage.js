import Head from "../elements/Head";
import Page from "../elements/Page";
import { PasswordResetForm } from "./../ui/inputs/Form";
import React from "react";

function PasswordResetPage() {
    return (
        <Page padding={true}>
            <Head title="Reset password" />
            <div className="grid-center margin-top--md">
                <PasswordResetForm />
            </div>
        </Page>
    );
}
export default PasswordResetPage;

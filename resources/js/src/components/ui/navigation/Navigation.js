import {
    ACCOUNT,
    HOME,
    LISTING,
    LOGIN,
    OVERVIEW,
    SAVED,
    SIGN_UP
} from "../../../constants/Routes";
import { DropDown, DropDownMenu } from "../surfaces/Dropdown";
import {
    IconBookmark,
    IconCompass,
    IconFlag,
    IconHome,
    IconMoon,
    IconSun,
    IconUser
} from "../content/Icon";
import { Link, NavLink } from "react-router-dom";
import {
    deauthenticateUser,
    isUserAuthenticated
} from "../../../helper/authHelper";

import { AppContext } from "../../../App";
import { Avatar } from "../content/Avatar";
import { ExternalLink } from "./CustomLink";
import { IconLogIn } from "./../content/Icon";
import { Line } from "../content/Line";
import React from "react";
import Space from "../content/WhiteSpace";
import Theme from "./../../../helper/themeHelper";
import { device } from "../../../constants/devices";
import notification from "./../../../helper/notificationHelper";
import styled from "styled-components";

function logUserOut() {
    deauthenticateUser();
    setTimeout(() => {
        location.reload();
    }, 2000);
}

const Nav = styled.ul`
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    top: 0px;
    z-index: 8;
    list-style-type: none;
    margin: 0;
    position: fixed;
    width: 100%;
    padding: 4px;
    background-color: var(--primary-color-2);
    border-bottom: 1px solid var(--light-grey);
    box-shadow: var(--primary-color) 0 -15px, rgba(0, 0, 0, 0.1) 0 0 15px;
`;

const NavItem = styled(NavLink)`
    float:right
    display: inline-flex;
    color: var(--grey);
    font-size: var(--text-rg);
    padding: 14px 16px;
    text-decoration: none;
    &:hover {
        color: var(--secondary-color);
    }
    @media ${device.tabletS} {
        display: none;
    }
`;

const NavItemLogo = styled(Link)`
    float: left;
    padding: 2px 20px;
    transform: translateY(4px);
`;

const NavAvatar = styled.span`
    float:right
    display: inline-flex;
    color: #6b6b6b;
    font-size: var(--text-rg);
    padding: 14px 16px;
    text-decoration: none;
    display: grid;
    justify-content: center;
    align-items: center;
    padding: 3px;

    @media ${device.tabletS} {
        display: initial;
    }
`;

const NavButton = styled.button`
    color: #fff;
    font-size: var(--text-rg);
    background: ${props => props.background || "var(--accent-color)"};
    border: transparent;
    border-radius: 5px;
    padding: 8px 16px;
    margin: -6px;
    &:hover {
        background: ${props => props.hover || `var(--accent-color-lighter)`};
    }

    @media ${device.tabletS} {
        display: initial;
    }
`;

/**
 * Main app navigation bar
 */
function Navigation(props) {
    return (
        <AppContext.Consumer>
            {context => (
                <nav>
                    <Nav>
                        <a
                            tabIndex="1"
                            style={{ textDecoration: "none" }}
                            href="#"
                            className="skip_link"
                        >
                            <span>
                                <b>Accessibility menu</b>
                                <hr />
                                <ul className="skip_intro">
                                    <li>
                                        ▲ up <Space amount={5} />:{" "}
                                        <kbd>Tab + Shift</kbd>
                                    </li>
                                    <li>
                                        ▼ down : <kbd>Tab</kbd>
                                    </li>
                                </ul>
                            </span>
                        </a>
                        <a tabIndex="1" href="#content" className="skip_link">
                            Skip to content <Space amount={2} />{" "}
                            <kbd>Enter</kbd>
                        </a>
                        <a tabIndex="1" href="/account" className="skip_link">
                            Account <Space amount={2} />
                            <kbd>Enter</kbd>
                        </a>
                        <NavItemLogo title="Enproperty" to={HOME} tabIndex="1">
                            <img
                                width="38"
                                hieght="38"
                                src={`${location.origin}/images/icons/icon-72x72.png`}
                            />
                        </NavItemLogo>
                        {props.authenticated ? (
                            <SignedInLinks
                                first_name={context.state.user.first_name}
                                last_name={context.state.user.last_name}
                                avatar={context.state.user.avatar}
                            />
                        ) : (
                            <SignedOutLinks />
                        )}
                    </Nav>
                    <div
                        aria-label="This is an early Beta access of Enproperty, we have polished up the platform but there might still be bugs and errors. Also we are looking forward to your feedback, you can give feed back by clicking the link above."
                        data-balloon-pos="down"
                        data-balloon-length="large"
                        style={{
                            background: "black",
                            position: "fixed",
                            textAlign: "center",
                            top: "56px",
                            padding: "2px",
                            width: "100%",
                            color: "white",
                            zIndex: 4
                        }}
                    >
                        Early access -{" "}
                        <ExternalLink
                            to="https://enproperty-co.typeform.com/to/Eyhj8j"
                            color="#0080FF"
                            hover="#00AAFF"
                        >
                            Feedback
                        </ExternalLink>
                    </div>
                </nav>
            )}
        </AppContext.Consumer>
    );
}

function SignedInLinks(props) {
    return (
        <React.Fragment>
            <NavAvatar
                style={{ cursor: "pointer" }}
                to={HOME}
                style={{ marginRight: "20px" }}
            >
                <DropDown hoverToOpen={false}>
                    <DropDownMenu x="-124px" y="58px" width="162px">
                        <DropDownItem
                            name={props.first_name + " " + props.last_name}
                        />
                    </DropDownMenu>
                    <Avatar
                        first_name={props.first_name}
                        last_name={props.last_name}
                        size="medium"
                        src={props.avatar}
                    />
                </DropDown>
            </NavAvatar>

            <NavItem to={SAVED} activeClassName="active" tabIndex="4">
                Saved
            </NavItem>
            <NavItem to={LISTING} activeClassName="active" tabIndex="3">
                {" "}
                Listing
            </NavItem>
            <NavItem to={OVERVIEW} activeClassName="active" tabIndex="2">
                Overview
            </NavItem>
        </React.Fragment>
    );
}

function SignedOutLinks() {
    return (
        <React.Fragment>
            <NavItem tabIndex="4" style={{ display: "initial" }} to={SIGN_UP}>
                <NavButton background="var(--black)">Sign up</NavButton>
            </NavItem>
            <NavItem
                style={{ display: "inline-flex" }}
                activeClassName="active"
                to={LOGIN}
                tabIndex="3"
                exact
            >
                Login
            </NavItem>
            <NavItem exact activeClassName="active" to={LISTING} tabIndex="2">
                Listing
            </NavItem>
        </React.Fragment>
    );
}

function DropDownItem(props) {
    return (
        <React.Fragment>
            <li
                style={{ cursor: "initial" }}
                className=" margin-bottom--xxs margin-top--none"
            >
                <b>{props.name}</b>
            </li>
            {/* <hr /> */}
            <Line />
            <li>
                <NavLink tabIndex="0" exact to={ACCOUNT}>
                    <IconUser yAxis={-1} width="24" heigh="24" />
                    <Space amount={2} /> Account
                </NavLink>
            </li>
            <li>
                <NavLink onClick={() => Theme.set()} tabIndex="0" exact to="#">
                    {Theme.get() ? (
                        <IconSun yAxis={-1} width="24" heigh="24" />
                    ) : (
                        <IconMoon yAxis={-1} width="24" heigh="24" />
                    )}
                    <Space amount={2} /> Switch Theme
                </NavLink>
            </li>
            <li>
                <NavLink onClick={() => logUserOut()} tabIndex="0" exact to="#">
                    <IconLogIn yAxis={-1} width="24" heigh="24" />
                    <Space amount={2} /> Logout
                </NavLink>
            </li>
        </React.Fragment>
    );
}

const StyledBottomNavigation = styled.div`
    -webkit-transform: translate3d(0, 0, 0);
    transform: translate3d(0, 0, 0);
    bottom: 0px;
    border-top: 1px solid var(--light-grey);
    z-index: 3;
    list-style-type: none;
    margin: 0;
    position: fixed;
    width: 100%;
    background-color: var(--primary-color-2);
    display: none;

    > ul {
        padding: 4px 30px;
        list-style: none;
        margin-bottom: 4px;
        margin-top: 10px;
        display: flex;
        justify-content: space-between;
    }
    > ul > li {
        font-size: var(--text-lg);
    }
    > ul > li > a {
        color: var(--secondary-color);
    }
    > ul > li > a > div {
        transform: scale(0.85) translateY(-9px);
    }
    > ul > li > .active > .nav-text {
        visibility: initial;
    }

    @media ${device.tabletS} {
        display: initial;
    }
`;
export function BottomNavigation(props) {
    return (
        <AppContext.Consumer>
            {context => (
                <StyledBottomNavigation>
                    <ul>
                        <li>
                            <NavLink
                                to={isUserAuthenticated() ? OVERVIEW : HOME}
                                exact={isUserAuthenticated() ? false : true}
                                activeClassName="active"
                            >
                                <IconHome />
                            </NavLink>
                        </li>
                        <li>
                            <NavLink
                                to={LISTING}
                                exact
                                activeClassName="active"
                            >
                                <IconCompass />
                            </NavLink>
                        </li>
                        {isUserAuthenticated() ? (
                            <React.Fragment>
                                <li>
                                    <NavLink
                                        to={SAVED}
                                        exact
                                        activeClassName="active"
                                    >
                                        <IconBookmark />
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to={ACCOUNT}>
                                        <Avatar
                                            first_name={
                                                context.state.user.first_name
                                            }
                                            last_name={
                                                context.state.user.last_name
                                            }
                                            src={context.state.user.avatar}
                                        />
                                    </NavLink>
                                </li>
                            </React.Fragment>
                        ) : (
                            <li>
                                <NavLink
                                    to={SIGN_UP}
                                    exact
                                    activeClassName="active"
                                >
                                    <IconLogIn />
                                </NavLink>
                            </li>
                        )}
                    </ul>
                </StyledBottomNavigation>
            )}
        </AppContext.Consumer>
    );
}

export default Navigation;

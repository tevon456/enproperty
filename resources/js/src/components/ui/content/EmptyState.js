import { Link } from "react-router-dom";
import React from "react";

function EmptyState(props) {
    return (
        <div style={{ maxWidth: props.maxWidth || "400px" }}>
            <h4 className="text--center">{props.title}</h4>
            <p className="text--center">{props.message}</p>
            <Link
                to={props.link}
                style={{ width: "auto" }}
                className="btn btn--primary btn--full-width"
            >
                {props.linkText}
            </Link>
        </div>
    );
}
export default EmptyState;

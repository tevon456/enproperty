import {
    CurrencyInput,
    FileUpload,
    GenericField,
    Label,
    PasswordField,
    PhoneInput,
    ProfileImageField,
    ToggleInput,
    unMaskCurrency,
    unMaskPhone
} from "./Field";
import { Form, Formik } from "formik";
import {
    LOGIN,
    PASSWORD_RESET,
    SIGN_UP,
    TERMS
} from "../../../constants/Routes";
import {
    LoginSchema,
    PasswordResetSchema,
    PropertySchema,
    SignupSchema,
    UnitSchema,
    UpdatePasswordSchema,
    UserCellSchema,
    UserEmailSchema,
    UserNameSchema,
    VerificationCodeSchema
} from "../../../helper/validation";
import {
    createProperty,
    createUnit,
    forgotPassword,
    login,
    requestVerification,
    signUp,
    updatePassword,
    updateProperty,
    updateUnit,
    updateUser,
    uploadProfileImage,
    uploadUnitImage,
    verifyCode
} from "../../../helper/apiHelper";
import { del, get } from "idb-keyval";

import CustomLink from "../navigation/CustomLink";
import { FORMS_COPY } from "../../../constants/copy";
import Note from "../content/Note";
import React from "react";
import { StyledToggleInputWrapper } from "./Field";
import { authenticateUser } from "../../../helper/authHelper";
import { device } from "../../../constants/devices";
import notification from "../../../helper/notificationHelper";
import styled from "styled-components";

export const OverideForm = styled.div`
    & form {
        border: 1px solid transparent;
        padding: 0px;
    }
    & form > button {
        margin-top: 24px;
        margin-bottom: 24px;
    }
`;

const StyledForm = styled(Form)`
    background-color: var(--primary-color-2);
    border: 1px solid #e0e6e8;
    border-radius: var(--radius);
    padding: 20px;
    margin-top: 30px;
    max-width: 500px;
    min-width: 400px;
    > input,
    > select,
    > div > input,
    > div > div > input,
    > textarea,
    > div > textarea,
    > div > div > textarea {
        width: 100%;
        display: block;
        font-size: var(--text-sm);
        padding: 12px 20px;
        margin: 4px 0px 18px 0px;
        box-sizing: border-box;
        border: 2px solid #ccc;
        border-radius: 6px;
        -webkit-transition: 0.5s;
        transition: 0.5s;
        outline: none;
    }
    textarea {
        max-width: 100%;
        min-height: 200px;
    }
    > input:focus,
    > div > input:focus,
    > div > div > input:focus,
    > textarea:focus,
    > div > textarea:focus,
    > div > div > textarea:focus {
        border: 2px solid var(--secondary-color);
    }

    select:focus {
        border: 2px solid var(--secondary-color);
    }

    > ::placeholder {
        color: var(--light-grey);
    }

    @media ${device.tablet} {
        min-width: 300px;
    }

    @media ${device.mobileL} {
        min-width: 240px;
        max-width: 300px;
    }
`;

const StyledFormFlex = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between !important;
`;

const StyledFormInline = styled.div`
    display: inline-block !important;
    padding: auto;import notification from './../../helper/notificationHelper';
import { UserUpdateSchema } from './../../helper/validation';
import { yup } from 'yup';
import Thumb from './Thumb';
import { values } from '../../../../../public/js/app';
import { UserCellSchema } from './../../../helper/validation';
import { Note } from '../content/Note';

`;

export const SignUpForm = () => (
    <Formik
        validationSchema={SignupSchema}
        // initialValues={{ role: null }}
        onSubmit={(values, { setSubmitting }) => {
            values.password_confirmation = values.password;
            values.first_name = values.firstName;
            values.last_name = values.lastName;
            delete values.firstName;
            delete values.lastName;
            delete values.show;
            signUp(values)
                .then(res => {
                    notification.success(res.data.message);
                    // location push login page
                })
                .catch(function(error) {
                    if (error.response) {
                        let signError = error.response.data.errors;
                        if (signError.email) {
                            notification.danger(signError.email[0]);
                        } else if (signError.name) {
                            notification.danger(signError.name[0]);
                        } else {
                            notification.danger(error.response.message);
                        }
                    }
                });
            setTimeout(() => {
                setSubmitting(false);
            }, 5000);
        }}
    >
        {({
            errors,
            touched,
            values,
            isSubmitting,
            setFieldValue,
            isValid
        }) => (
            <StyledForm autoComplete="off">
                <h3>Sign up</h3>
                <StyledFormFlex>
                    <StyledFormInline>
                        <GenericField
                            label="First Name"
                            name="firstName"
                            errors={errors.firstName}
                            touched={touched.firstName}
                        />
                    </StyledFormInline>
                    <StyledFormInline>
                        <GenericField
                            label="Last Name"
                            name="lastName"
                            errors={errors.lastName}
                            touched={touched.lastName}
                        />
                    </StyledFormInline>
                </StyledFormFlex>
                <GenericField
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="eg. jean@gmail.com"
                    errors={errors.email}
                    touched={touched.email}
                />
                <PasswordField
                    submit={isSubmitting}
                    values={values}
                    errors={errors}
                    touched={touched}
                />
                <Label
                    className="margin-top--sm"
                    aria-label="A lister is someone who list their property for others to rent"
                    data-balloon-pos="right"
                    data-balloon-length="medium"
                >
                    I'm registering as a
                </Label>
                <StyledToggleInputWrapper className="margin-top--xxxs margin-bottom--sm">
                    <ToggleInput
                        id="true"
                        value={values.role}
                        name="role"
                        type="radio"
                        checked={values.role === "landlord"}
                        label="Rental Lister"
                        onChange={() => {
                            setFieldValue("role", "landlord");
                        }}
                    />
                    <ToggleInput
                        id="false"
                        type="radio"
                        value={values.role}
                        name="role"
                        checked={values.role === "user"}
                        label="Renter"
                        onChange={() => {
                            setFieldValue("role", "user");
                        }}
                    />
                </StyledToggleInputWrapper>
                <div className="btns">
                    <button
                        disabled={isSubmitting || isValid === false}
                        className="btn btn--primary btn--full-width "
                        type="submit"
                    >
                        {isSubmitting ? "Loading" : "Submit"}
                    </button>
                </div>

                <p className="text--sm ">
                    Already have an account?{" "}
                    <CustomLink to={LOGIN}>Login</CustomLink>. By continuing,
                    you accept our{" "}
                    <CustomLink to={TERMS}>Terms of Service</CustomLink>{" "}
                </p>
            </StyledForm>
        )}
    </Formik>
);

export const LoginForm = () => (
    <Formik
        validationSchema={LoginSchema}
        onSubmit={(values, { setSubmitting }) => {
            delete values.show;
            login(values)
                .then(res => {
                    authenticateUser(res.data.access_token);
                    location.reload();
                })
                .catch(function(error) {
                    if (error.response) {
                        notification.danger(error.response.data.message);
                    }
                });
            setTimeout(() => {
                setSubmitting(false);
            }, 7000);
        }}
    >
        {({ errors, touched, values, isSubmitting }) => (
            <StyledForm autoComplete="on">
                <h3>Login</h3>
                <GenericField
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="eg. jean@gmail.com"
                    errors={errors.email}
                    touched={touched.email}
                />
                <PasswordField
                    submit={isSubmitting}
                    values={values}
                    errors={errors}
                    touched={touched}
                />
                <button
                    disabled={isSubmitting}
                    className="btn btn--primary btn--full-width margin-top--md"
                    type="submit"
                >
                    {isSubmitting ? "Loading" : "Login"}
                </button>
                <p className="text--sm">
                    Don't have an account?{" "}
                    <CustomLink to={SIGN_UP}>Sign up</CustomLink> or{" "}
                    <CustomLink to={PASSWORD_RESET}>Forgot password</CustomLink>
                </p>
            </StyledForm>
        )}
    </Formik>
);

export const PasswordResetForm = () => (
    <Formik
        initialValues={{
            email: ""
        }}
        validationSchema={PasswordResetSchema}
        onSubmit={(values, { setSubmitting }) => {
            forgotPassword(values)
                .then(res => {
                    notification.success(res.data.message);
                })
                .catch(function(error) {
                    if (error.response) {
                        notification.danger(error.response.data.message);
                    }
                });
            setTimeout(() => {
                setSubmitting(false);
            }, 2000);
        }}
    >
        {({ errors, touched, isSubmitting }) => (
            <StyledForm>
                <h3>Reset Password</h3>
                <GenericField
                    label="Email"
                    name="email"
                    type="email"
                    placeholder="eg. jean@gmail.com"
                    errors={errors.email}
                    touched={touched.email}
                />
                <button
                    disabled={isSubmitting}
                    className="btn btn--primary btn--full-width margin-top--md"
                    type="submit"
                >
                    {isSubmitting ? "Loading" : "Reset Password"}
                </button>
                <p className="text--sm">
                    <CustomLink to={LOGIN}>Back to login</CustomLink>
                </p>

                <Note>
                    <p className="text--sm">
                        {FORMS_COPY.password_reset.instruction}
                    </p>
                </Note>
            </StyledForm>
        )}
    </Formik>
);

export const UpdatePasswordForm = props => (
    <Formik
        validationSchema={UpdatePasswordSchema}
        initialValues={{
            password: ""
        }}
        onSubmit={(values, { setSubmitting }) => {
            get("reset")
                .then(res => {
                    values.email = res.email;
                    values.token = props.match.params.token;
                    values.password_confirmation = values.password;

                    updatePassword(values)
                        .then(res => {
                            del("reset");
                            notification.success("Password updated");
                            setTimeout(() => {
                                // setSubmitting(false);
                            }, 2000);
                        })
                        .catch(error => {
                            if (error.response) {
                                notification.danger(
                                    error.response.data.message
                                );
                                del("reset");
                            }
                        });
                })
                .catch(() => {
                    notification.danger("This reset token is invalid");
                });
            setTimeout(() => {
                setSubmitting(false);
            }, 2000);
        }}
    >
        {({ errors, values, touched, isSubmitting, isValid }) => (
            <StyledForm>
                <h3>New Password</h3>
                <PasswordField
                    submit={isSubmitting}
                    values={values}
                    errors={errors}
                    touched={touched}
                />
                <button
                    disabled={isSubmitting || !isValid}
                    className="btn btn--primary btn--full-width margin-top--md"
                    type="submit"
                >
                    {isSubmitting ? "Loading" : "Update Password"}
                </button>
                <p className="text--sm">
                    <CustomLink to={LOGIN}>Back to login</CustomLink>
                </p>
            </StyledForm>
        )}
    </Formik>
);

export function UpdateNameForm(props) {
    return (
        <Formik
            validationSchema={UserNameSchema}
            initialValues={{ firstName: "", lastName: "" }}
            onSubmit={(values, { setSubmitting }) => {
                values.first_name = values.firstName;
                values.last_name = values.lastName;
                values.cell = props.data.cell;
                delete values.firstName;
                delete values.lastName;
                setTimeout(() => {
                    updateUser(values)
                        .then(res => {
                            notification.success(res.data.message, 8000);
                            del("user").then(() => {
                                location.reload();
                            });
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response);
                            }
                        });
                    setTimeout(() => {
                        setSubmitting(false);
                    }, 2000);
                }, 3000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm>
                    <StyledFormFlex>
                        <StyledFormInline>
                            <GenericField
                                label="First Name"
                                name="firstName"
                                errors={errors.firstName}
                                touched={touched.firstName}
                            />
                        </StyledFormInline>
                        <StyledFormInline>
                            <GenericField
                                label="Last Name"
                                name="lastName"
                                errors={errors.lastName}
                                touched={touched.lastName}
                            />
                        </StyledFormInline>
                    </StyledFormFlex>

                    <button
                        disabled={isSubmitting || isValid === false}
                        className="btn btn--primary btn--sm "
                        type="submit"
                    >
                        {isSubmitting ? "Saving" : "Save"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}

export function UpdateCellForm(props) {
    return (
        <Formik
            validationSchema={UserCellSchema}
            initialValues={{ contact_number: props.cell }}
            onSubmit={(values, { setSubmitting }) => {
                values.contact_number = unMaskPhone(values.contact_number);
                localStorage.setItem("verification", values.contact_number);
                requestVerification(values)
                    .then(res => {
                        notification.info(res.data.message, 8000);
                    })
                    .catch(error => {
                        if (error.response) {
                            console.log(error.response);
                        }
                    });
                setTimeout(() => {
                    setSubmitting(false);
                }, 2000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm>
                    <GenericField
                        label="Cellular phone number"
                        name="contact_number"
                        errors={errors.contact_number}
                        placeholder="Eg +1 (876) 123-4567"
                        touched={touched.contact_number}
                        component={PhoneInput}
                    />
                    <div className="btns">
                        <button
                            disabled={isSubmitting || isValid == false}
                            className="btn btn--primary btn--sm "
                            type="submit"
                        >
                            {isSubmitting ? "Saving" : "Send Code"}
                        </button>
                    </div>
                </StyledForm>
            )}
        </Formik>
    );
}

export function VerificationCodeForm() {
    return (
        <Formik
            validationSchema={VerificationCodeSchema}
            initialValues={{
                contact_number: localStorage.getItem("verification"),
                code: ""
            }}
            onSubmit={(values, { setSubmitting }) => {
                console.log(values);
                verifyCode(values)
                    .then(res => {
                        notification.success(res.data.message, 8000);
                        localStorage.removeItem("verification");
                        del("user").then(() => {
                            location.reload();
                        });
                    })
                    .catch(error => {
                        if (error.response) {
                            console.log(error.response);
                        }
                    });
                setTimeout(() => {
                    setSubmitting(false);
                }, 2000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm>
                    <GenericField
                        label={`Verification code`}
                        name="code"
                        maxWidth="200px"
                        placeholder="Enter the 6 digit code"
                        errors={errors.code}
                        touched={touched.code}
                        type="number"
                    />
                    <div className="btns">
                        <button
                            disabled={isSubmitting || isValid == false}
                            className="btn btn--primary btn--sm "
                            type="submit"
                        >
                            {isSubmitting ? "Checking" : "Verify"}
                        </button>
                    </div>
                </StyledForm>
            )}
        </Formik>
    );
}

export function UpdateEmailForm(props) {
    return (
        <Formik
            validationSchema={UserEmailSchema}
            initialValues={{ email: props.email }}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    updateUser(values)
                        .then(res => {
                            notification.success(res.data.message, 5000);
                            del("user").then(() => {
                                location.reload();
                            });
                        })
                        .catch(error => {
                            if (error.response) {
                                console.log(error.response);
                            }
                        });
                    setTimeout(() => {
                        setSubmitting(false);
                    }, 2000);
                }, 3000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm>
                    <GenericField
                        label="Email"
                        name="email"
                        type="email"
                        placeholder="eg. jean@gmail.com"
                        errors={errors.email}
                        touched={touched.email}
                    />
                    <button
                        disabled={isSubmitting || isValid == false}
                        className="btn btn--primary btn--sm "
                        type="submit"
                    >
                        {isSubmitting ? "Saving" : "Save"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}

export function PropertyForm(props) {
    return (
        <Formik
            validationSchema={props.update ? null : PropertySchema}
            initialValues={
                props.update
                    ? {
                          property_title: props.update.property_title,
                          property_id: props.update.uuid,
                          property_description:
                              props.update.property_description,
                          street: props.update.street,
                          city: props.update.city,
                          state: props.update.state
                      }
                    : {
                          property_title: "",
                          property_description: "",
                          street: "",
                          city: "",
                          state: ""
                      }
            }
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    function Create(values) {
                        createProperty(values)
                            .then(res => {
                                notification.success("Property added", 5000);
                                {
                                    props.closeModal
                                        ? props.closeModal()
                                        : null;
                                }
                            })
                            .catch(error => {
                                if (error.response) {
                                    notification.warning(
                                        error.response.data.message,
                                        5000
                                    );
                                    props.closeModal
                                        ? props.closeModal()
                                        : null;
                                }
                            });
                    }

                    function Update(values) {
                        updateProperty(values).then(() => {
                            notification.success("Property updated", 5000);
                            {
                                props.closeModal ? props.closeModal() : null;
                            }
                        });
                    }

                    props.update ? Update(values) : Create(values);

                    setTimeout(() => {
                        setSubmitting(false);
                    }, 2000);
                }, 3000);
            }}
        >
            {({ errors, touched, isSubmitting, isValid }) => (
                <StyledForm id="s4">
                    <GenericField
                        label="Title"
                        name="property_title"
                        placeholder="eg. Double Bedroom shared facilities"
                        errors={errors.property_title}
                        touched={touched.property_title}
                    />
                    <GenericField
                        label="Description"
                        placeholder="Start Writing here"
                        name="property_description"
                        errors={errors.property_description}
                        touched={touched.property_description}
                        component="textarea"
                    />
                    <GenericField
                        label="Street"
                        name="street"
                        errors={errors.street}
                        touched={touched.street}
                    />
                    <GenericField
                        label="Town / City"
                        name="city"
                        errors={errors.city}
                        touched={touched.city}
                    />
                    <GenericField
                        label="Parish"
                        name="state"
                        errors={errors.state}
                        touched={touched.state}
                    />
                    <button
                        disabled={isSubmitting || isValid === false}
                        className="btn btn--primary btn--full-width "
                        type="submit"
                    >
                        {isSubmitting
                            ? "Loading"
                            : props.update
                            ? "Save changes"
                            : "Add Property"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}

export function UnitForm(props) {
    return (
        <Formik
            validationSchema={props.update ? null : UnitSchema}
            initialValues={
                props.update
                    ? {
                          unit_title: props.update.unit_title,
                          unit_description: props.update.unit_description,
                          price_fee: props.update.price_fee,
                          security_deposit: props.update.security_deposit,
                          listed: props.update.listed,
                          property_id: props.update.parent_property.id,
                          unit_id: props.update.uuid
                      }
                    : {
                          unit_title: "",
                          unit_description: "",
                          price_fee: "",
                          security_deposit: "0",
                          listed: true,
                          property_id: props.data.id
                      }
            }
            onSubmit={(values, { setSubmitting }) => {
                if (
                    values.security_deposit != null ||
                    values.security_deposit != undefined
                ) {
                    values.security_deposit = unMaskCurrency(
                        values.security_deposit
                    );
                }

                values.price_fee = unMaskCurrency(values.price_fee);
                function Update(data) {
                    updateUnit(data).then(res => {
                        {
                            data.file
                                ? File(data.file, res.data)
                                : notification.success("Unit updated", 5000);
                        }
                        {
                            props.closeModal ? props.closeModal() : null;
                        }
                    });
                }
                function Create(data) {
                    createUnit(data)
                        .then(res => {
                            {
                                data.file
                                    ? File(data.file, res.data)
                                    : notification.success("Unit added", 5000);
                            }
                            {
                                props.closeModal ? props.closeModal() : null;
                            }
                        })
                        .catch(error => {
                            if (error.response) {
                                notification.warning(
                                    error.response.data.message,
                                    12000
                                );
                                {
                                    props.closeModal
                                        ? props.closeModal()
                                        : null;
                                }
                            }
                        });
                }
                function File(file, id) {
                    let formData = new FormData();
                    for (let i = 0; i < file.length; i++) {
                        formData.append(`file[${[i]}]`, file[i]);
                    }
                    formData.append("id", id);
                    uploadUnitImage(formData).then(res => {
                        notification.success("Success", 8000);
                        setSubmitting(false);
                    });
                }
                function Errors(message) {
                    notification.danger(message, 8000);
                    setSubmitting(false);
                }

                //if files are selected start validating
                if (values.file) {
                    const config = {
                        maxSize: 2,
                        fileLimit: 6,
                        compress: true,
                        extensions: ["image/png", "image/jpeg"]
                    };
                    FileValidate(values.file, config).then(res => {
                        values.file = res.file;
                        res.error
                            ? Errors(res.message)
                            : props.update
                            ? Update(values)
                            : Create(values);
                    });
                }
                // if no files then do a regular create or update
                else {
                    {
                        props.update ? Update(values) : Create(values);
                        setTimeout(() => {
                            setSubmitting(false);
                        }, 1000);
                    }
                }
            }}
        >
            {({
                errors,
                touched,
                isSubmitting,
                isValid,
                values,
                setFieldValue,
                handleBlur,
                handleChange
            }) => (
                <StyledForm>
                    {props.update ? null : (
                        <React.Fragment>
                            <Label className="margin-bottom--lg">
                                Property to add unit
                            </Label>
                            <select
                                name="property_id"
                                className=" margin-bottom--sm margin-top--sm"
                                value={values.property_id}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                style={{
                                    fontWeight: "normal",
                                    fontSize: "var(--text-sm)"
                                }}
                            >
                                <option />
                                {props.data.map(d => (
                                    <option key={d.uuid} value={d.uuid}>
                                        {d.property_title}
                                    </option>
                                ))}
                            </select>
                        </React.Fragment>
                    )}
                    <GenericField
                        label="Title"
                        name="unit_title"
                        placeholder="eg. Double Bedroom shared facilities"
                        errors={errors.unit_title}
                        touched={touched.unit_title}
                    />
                    <GenericField
                        label="Description [optional]"
                        placeholder="Start Writing here"
                        name="unit_description"
                        errors={errors.unit_description}
                        touched={touched.unit_description}
                        component="textarea"
                    />
                    <GenericField
                        label="Price"
                        name="price_fee"
                        component={CurrencyInput}
                        errors={errors.price_fee}
                        touched={touched.price_fee}
                    />
                    <GenericField
                        label="Deposit [optional]"
                        name="security_deposit"
                        component={CurrencyInput}
                        errors={errors.security_deposit}
                        touched={touched.security_deposit}
                    />
                    <Label>List this unit</Label>
                    <StyledToggleInputWrapper className="margin-top--xs">
                        <ToggleInput
                            id="true"
                            value={values.listed}
                            name="listed"
                            type="radio"
                            checked={values.listed === true}
                            label="Yes"
                            onChange={() => {
                                setFieldValue("listed", true);
                            }}
                        />
                        <ToggleInput
                            id="false"
                            className="no"
                            type="radio"
                            value={values.listed}
                            name="listed"
                            checked={values.listed === false}
                            label="No"
                            onChange={() => {
                                setFieldValue("listed", false);
                            }}
                        />
                    </StyledToggleInputWrapper>

                    <Label className="margin-top--xs">
                        {props.update ? "Add more photos" : "Photos"}{" "}
                    </Label>

                    <FileUpload value={values} set={setFieldValue} />

                    <button
                        disabled={isSubmitting || isValid === false}
                        className="btn btn--primary btn--full-width "
                        type="submit"
                    >
                        {isSubmitting
                            ? "Loading"
                            : props.update
                            ? "Save changes"
                            : "Add Unit"}
                    </button>
                </StyledForm>
            )}
        </Formik>
    );
}

export function ProfileImageUpload(props) {
    return (
        <Formik
            validationSchema={null}
            initialValues={{ avatar: null }}
            onSubmit={(values, { setSubmitting }) => {
                const config = {
                    maxSize: 2,
                    fileLimit: 1,
                    compress: true,
                    extensions: ["image/png", "image/jpeg"]
                };
                FileValidate(values.avatar, config)
                    .then(res => {
                        let formData = new FormData();
                        formData.append("avatar", res.file[0]);
                        res.error
                            ? notification.danger(res.message)
                            : uploadProfileImage(formData).then(res => {
                                  del("user");
                                  location.reload();
                              });
                    })
                    .catch(err => {
                        console.log(err);
                    });
                setTimeout(() => {
                    setSubmitting(false);
                }, 2000);
            }}
        >
            {({ values, setFieldValue, isSubmitting, isValid }) => (
                <StyledForm style={{ border: "none" }}>
                    <ProfileImageField value={values} set={setFieldValue} />
                    <div style={{ display: "flex", justifyContent: "center" }}>
                        <button
                            disabled={isSubmitting || isValid === false}
                            className="btn btn--blue btn--sm margin-top--sm"
                            type="submit"
                        >
                            {isSubmitting ? "Saving" : "Save"}
                        </button>
                    </div>
                </StyledForm>
            )}
        </Formik>
    );
}

export function FileUploadForm(props) {
    return (
        <Formik
            validationSchema={null}
            initialValues={{ files: null }}
            onSubmit={(values, { setSubmitting }) => {
                const config = {
                    maxSize: 2,
                    fileLimit: 1,
                    compress: true,
                    extensions: ["image/png", "image/jpeg"]
                };

                function File(file, id) {
                    let formData = new FormData();
                    for (let i = 0; i < file.length; i++) {
                        formData.append(`file[${[i]}]`, file[i]);
                    }
                    formData.append("id", id);
                    uploadUnitImage(formData).then(() => {
                        notification.info("New images added");
                        props.close();
                        setSubmitting(false);
                    });
                }
                FileValidate(values.file, config).then(res => {
                    File(res.file, props.data.uuid);
                });
                setTimeout(() => {
                    setSubmitting(false);
                }, 2000);
            }}
        >
            {({ values, setFieldValue, isSubmitting, isValid }) => (
                <StyledForm style={{ border: "none", marginTop: "0px" }}>
                    <FileUpload value={values} set={setFieldValue} />
                    <div style={{ display: "flex", justifyContent: "center" }}>
                        <button
                            disabled={isSubmitting || isValid === false}
                            className="btn btn--blue btn--full-width margin-top--sm"
                            type="submit"
                        >
                            {isSubmitting ? "Saving" : "Upload"}
                        </button>
                    </div>
                </StyledForm>
            )}
        </Formik>
    );
}

/**
 * Validate files and resturns an object with message and files
 *
 * @param {object} file - A file object
 * @param {object} config - object containing validation configurations
 * @return {object} returns an object
 *
 * @example
 *     const config = {
 *       maxSize: 2, // Maximum size of individual files in mb
 *       fileLimit: 1, // Maximum number of files allowed
 *       compress: false, // Option to compress files only works for images
 *       extensions: ["image/png", "image/jpeg"] // an array of allowed mimetypes
 *     };
 *
 *     FileValidate(file, config)
 *
 */
async function FileValidate(file, config) {
    //Init a validation object
    let Validation = {
        error: false,
        message: "",
        file: null
    };

    let conversion = config.maxSize * 1048576;

    let messages = [
        "no arguments passed",
        "file object expexted",
        `max number of files are ${config.filelimit} you provided ${file.length}`,
        `max size is ${config.maxSize} mb `,
        `that file type is not allowed`,
        "file validate success",
        "select at least one image "
    ];

    /**
     * Ensures all files are equal to or below maxSize in mb else return an object with error mesasge
     *
     * @param {object} file - A file object
     * @return {object} returns an object
     *
     */
    function FileSize(file) {
        for (let i = 0; i < file.length; i++) {
            if (file[i].size > conversion) {
                Validation.error = true;
                Validation.message =
                    messages[3] + `, '${file[i].name}' is too large`;
                return Validation;
            }
        }
    }

    /**
     * Ensures all files have specified mimetypes else return an object with error mesasge
     *
     * @param {object} file - A file object
     * @return {object} returns an object
     *
     */
    function FileExtension(file, extension) {
        for (let i = 0; i <= file.length - 1; i++) {
            if (extension.includes(file[i].type) == false) {
                Validation.error = true;
                Validation.message = messages[4] + `, "${file[i].name}"`;
                return Validation;
            }
        }
    }

    /**
     * Async function that compresses image files
     *
     * @param {object} file - A file object
     * @return {object} returns an object
     *
     */
    async function FileImageCompression(file) {
        // initialize compress
        const Compress = require("compress.js");
        const compress = new Compress();
        const compressfile = await compress
            .compress(file, {
                size: 2, // the max size in MB, defaults to 2MB
                quality: 0.55, // the quality of the image, max is 1,
                maxWidth: 1920, // the max width of the output image, defaults to 1920px
                maxHeight: 1920, // the max height of the output image, defaults to 1920px
                resize: true // defaults to true, set false if you do not want to resize the image width and height
            })
            .then(results => {
                let newFile = [];
                const img = [];
                for (let i = 0; i <= results.length - 1; i++) {
                    img[i] = results[i];
                    const base64 = img[i].data;
                    const imgExt = img.ext;
                    newFile[i] = Compress.convertBase64ToFile(base64, imgExt);
                }
                return newFile;
            })
            .catch(err => {
                console.log(err);
            });
        return compressfile;
    }

    //Ensure a param was passed to file param
    if (file == undefined || file == null) {
        Validation.error = true;
        Validation.message = messages[0];
        return Validation;
    }
    //Checks that file is of type object
    else if (typeof file != "object") {
        Validation.error = true;
        Validation.message = messages[1];
        return Validation;
    }
    //Check that amount of files meet fileLimit requirement
    else if (file.length > config.filelimit) {
        Validation.error = true;
        Validation.message = messages[2];
        return Validation;
    }
    //If no files are in the file object return an error message in return object
    else if (file.length == 0) {
        Validation.error = true;
        Validation.message = messages[6];
        return Validation;
    }
    // Check size in mb
    else if (FileSize(file) != undefined) {
        return FileSize(file);
    }
    //Check for extensions in config then validate against
    else if (FileExtension(file, config.extensions) != undefined) {
        return FileExtension(file, config.extensions);
    }
    //At this point all validation checks have passed
    else {
        //Compress image files
        if (config.compress == true) {
            const filereturn = await FileImageCompression(file).then(res => {
                Validation.file = res;
                Validation.error = false;
                Validation.message = messages[5];
                return Validation;
            });
            return filereturn;
        } else {
            Validation.file = file;
            Validation.error = false;
            Validation.message = messages[5];
            return Validation;
        }
    }
}

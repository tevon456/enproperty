import React from "react";
import styled from "styled-components";

const Landing = styled.a`
    width: 1px;
    height: 0px;
    position: absolute;
    opacity: 0;
    background: transparent;
    overflow: hidden;
    display: inline-block;
    z-index: 2;

    &:focus {
        width: auto;
        height: auto;
        opacity: 1;
    }
`;
const SkipLanding = props => (
    <Landing tabIndex="0" name={props.name}>
        <kbd>{props.name}</kbd>
    </Landing>
);
export default SkipLanding;

import { Helmet } from "react-helmet";
import React from "react";

function Head(props) {
    let title = "Enproperty - Simple rental listing";
    let link = "https://enproperty.co";
    let keyWords =
        "student rentals, Jamaica, free property listing, property listing, enproperty ";
    let description =
        "Simple free rental and property listing for Jamaica, great for student accomodations and rentals";
    let image = "";

    return (
        <Helmet>
            <title>{props.title}</title>
            <meta name="title" content={props.title || title} />
            <meta
                name="description"
                content={props.description || description}
            />
            <meta name="keywords" content={props.keyWords || keyWords} />
            <meta name="image" content={props.image} />

            {/* <!-- Open Graph / Facebook --> */}
            <meta property="og:type" content="website" />
            <meta property="og:url" content={location.href || link} />
            <meta property="og:title" content={props.title || title} />
            <meta
                property="og:description"
                content={props.description || description}
            />
            <meta property="og:image" content={props.image} />

            {/* <!-- Twitter --> */}
            <meta property="twitter:card" content="summary_large_image" />
            <meta property="twitter:url" content={location.href || link} />
            <meta property="twitter:title" content={props.title || title} />
            <meta
                property="twitter:description"
                content={props.description || description}
            />
            <meta
                property="twitter:image"
                content={props.image || image}
            ></meta>
        </Helmet>
    );
}

export default Head;

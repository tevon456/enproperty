import { Authorization } from "./../../helper/authHelper";
import React from "react";

function Can(props) {
    return (
        <React.Fragment>
            {Authorization.roleHasPermission(props.i)
                ? props.children
                : props.else
                ? props.else
                : null}
        </React.Fragment>
    );
}

export default Can;

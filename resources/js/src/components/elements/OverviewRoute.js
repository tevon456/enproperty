import { OVERVIEW, PROPERTY, UNIT } from "../../constants/Routes";
import React, { Component, lazy } from "react";
import { Route, Switch } from "react-router-dom";
import { getUserProperties, getUserUnits } from "../../helper/apiHelper";

import { AppContext } from "./../../App";
import { Authorization } from "../../helper/authHelper";
import ErrorBoundary from "../ui/content/Error";
import GuardRoute from "./GuardRoute";
import NotFound from "./../pages/NotFound";
import Overview from "./../pages/LandLordOverview";
import { SubNav } from "./../ui/navigation/SubNavigation";

// const Overview = lazy(() => import("./../pages/LandLordOverview"));
const Property = lazy(() => import("./../pages/LandLordProperty"));
const Unit = lazy(() => import("./../pages/LandLordUnit"));
const Saved = lazy(() => import("./../pages/Saved"));

export const OverviewContext = React.createContext();
export class OverviewProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userProperties: [],
            userUnits: [],
            isLoading: true
        };
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        this.getData();
    }

    getData() {
        {
            Authorization.roleHasPermission(["create listing"])
                ? this.getLandLordData()
                : null;
        }
    }

    getLandLordData() {
        getUserProperties().then(res => {
            this.setState({
                isLoading:
                    this.state.userProperties === undefined ? true : false,
                userProperties: res.data
            });
        });
        getUserUnits().then(res => {
            this.setState({
                userUnits: res.data
            });
        });
    }

    render() {
        return (
            <OverviewContext.Provider
                value={{
                    state: this.state,
                    getData: this.getData
                }}
            >
                {this.props.children}
            </OverviewContext.Provider>
        );
    }
}

function LinkMap() {
    let linkmap = {};
    if (Authorization.roleHasPermission(["create listing", "edit listing"])) {
        linkmap = [
            { link: OVERVIEW, name: "Overview", exact: true },
            { link: OVERVIEW + PROPERTY, name: "My Properties", exact: true },
            { link: OVERVIEW + UNIT, name: "My Units", exact: true }
        ];
    } else {
        linkmap = [{ link: OVERVIEW, name: "Saved", exact: true }];
    }
    return linkmap;
}

function OverviewRoute() {
    return (
        <ErrorBoundary>
            <AppContext.Consumer>
                {context => (
                    <OverviewProvider>
                        <OverviewContext.Consumer>
                            {overview => (
                                <React.Fragment>
                                    <div className="margin-top--xxl" />
                                    <SubNav linkmap={LinkMap()} />
                                    <Switch>
                                        {Authorization.roleHasPermission([
                                            "create listing"
                                        ]) ? (
                                            <LandlordRoutes
                                                refresh={overview.getData}
                                                user={context.state.user}
                                            />
                                        ) : (
                                            <UserRoutes
                                                refresh={overview.getData}
                                                user={context.state.user}
                                            />
                                        )}
                                        <Route exact component={NotFound} />
                                    </Switch>
                                </React.Fragment>
                            )}
                        </OverviewContext.Consumer>
                    </OverviewProvider>
                )}
            </AppContext.Consumer>
        </ErrorBoundary>
    );
}

function LandlordRoutes(props) {
    return (
        <React.Fragment>
            <GuardRoute
                exact
                path={OVERVIEW}
                permission={["create listing", "create property"]}
                component={() => (
                    <Overview refresh={props.refresh} user={props.user} />
                )}
            />
            <GuardRoute
                path={OVERVIEW + PROPERTY}
                permission={["delete property", "edit property"]}
                component={() => (
                    <Property refresh={props.refresh} user={props.user} />
                )}
            />
            <GuardRoute
                path={OVERVIEW + UNIT}
                permission={["edit listing", "delete listing"]}
                component={() => (
                    <Unit refresh={props.refresh} user={props.user} />
                )}
            />
        </React.Fragment>
    );
}

function UserRoutes() {
    return (
        <React.Fragment>
            <Route exact path={OVERVIEW} render={() => <Saved />} />
        </React.Fragment>
    );
}

export default OverviewRoute;

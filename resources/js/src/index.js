import * as Sentry from "@sentry/browser";
import * as serviceWorker from "./serviceWorker";

import { BrowserRouter, Route } from "react-router-dom";

import App from "./App";
import React from "react";
import { render } from "react-dom";

const Root = () => (
    <BrowserRouter>
        <Route component={App} />
    </BrowserRouter>
);

Sentry.init({
    dsn: "https://12fe0c7b79a24830b66c46491264e67b@sentry.io/1773864"
});

if (document.getElementById("root")) {
    render(<Root />, document.getElementById("root"));
}
serviceWorker.register();

import "react-toastify/dist/ReactToastify.css";

import {
    ACCOUNT,
    ACTIVATE,
    HOME,
    ID,
    LISTING,
    LOGIN,
    OVERVIEW,
    PASSWORD_RESET,
    SAVED,
    SIGN_UP,
    TERMS,
    TOKEN,
    UPDATE
} from "./constants/Routes";
import { Authorization, isUserAuthenticated } from "./helper/authHelper";
import Navigation, {
    BottomNavigation
} from "./components/ui/navigation/Navigation";
import React, { Component, Suspense, lazy } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Slide, ToastContainer } from "react-toastify";
import { crispChat, getElementByXpath, googleTracking } from "./helper/lib";
import { get, set } from "idb-keyval";

import ErrorBoundary from "./components/ui/content/Error";
import GuardRoute from "./components/elements/GuardRoute";
import Home from "./components/pages/Home";
import Listing from "./components/pages/Listings";
import LoginPage from "./components/pages/LoginPage";
import NotFound from "./components/pages/NotFound";
import ProtectedRoute from "./components/elements/ProtectedRoute";
import SignUpPage from "./components/pages/SignUpPage";
import Theme from "./helper/themeHelper";
import { getUser } from "./helper/apiHelper";

const Account = lazy(() => import("./components/pages/Account"));
const Token = lazy(() => import("./components/pages/Token"));
const Test = lazy(() => import("./components/pages/Test"));
const OverviewRoute = lazy(() => import("./components/elements/OverviewRoute"));
const ListingPage = lazy(() => import("./components/pages/ListingPage"));
const TermsOfService = lazy(() => import("./components/pages/TermsOfService"));
const PrivacyPolicy = lazy(() => import("./components/pages/PrivacyPolicy"));
const Saved = lazy(() => import("./components/pages/Saved"));
const PasswordResetPage = lazy(() =>
    import("./components/pages/PasswordResetPage")
);
const UpdatePasswordPage = lazy(() =>
    import("./components/pages/UpdatePasswordPage")
);

export const AppContext = React.createContext();
class AppProvider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: []
        };
        this.setState = this.setState.bind(this);
    }
    componentDidMount() {
        this.getAuthenticatedUser();
    }

    getAuthenticatedUser() {
        if (isUserAuthenticated()) {
            //try to get user from cache
            get("user").then(user => {
                //If user not available
                if (user === undefined) {
                    //use getUser api call to retrieve user
                    getUser().then(res => {
                        // set user to state and context
                        this.setState({ user: res.data });
                        //store the user to cache
                        set("user", res.data);
                        Authorization.set();
                    });
                } else {
                    this.setState({ user: user });
                }
            });
        }
    }

    render() {
        return (
            <AppContext.Provider
                //Ensure these values are available app wide via context
                value={{
                    state: this.state,
                    getListing: this.getListing
                }}
            >
                {this.props.children}
            </AppContext.Provider>
        );
    }
}

class App extends Component {
    constructor(props) {
        super(props);
        this.setState = this.setState.bind(this);
    }
    componentDidMount() {
        this.storageEvent();
        crispChat("c02213de-7fcc-4a3e-a259-07cc38d27412");
        Theme.apply();
    }
    //Sync sign in or sign out across browser tabs
    storageEvent() {
        window.addEventListener("storage", function(e) {
            let s = localStorage.getItem("sync");
            if (s == "out" || s == null || s == "in") {
                location.reload();
            }
        });
        window.onstorage = () => {
            Authorization.tamper();
        };
    }

    render() {
        googleTracking("UA-143813982-2");
        return (
            <ErrorBoundary>
                <AppProvider>
                    <Navigation authenticated={isUserAuthenticated()} />
                    <BottomNavigation />
                    <div style={{ margin: 0 }}>
                        <Suspense
                            fallback={
                                <div className="margin-top--xxxl">
                                    <h4 className="text--center">Loading...</h4>
                                </div>
                            }
                        >
                            <Switch>
                                <Route
                                    exact
                                    path={LISTING}
                                    component={Listing}
                                />
                                <Route
                                    exact
                                    path={LISTING + `/page/:number`}
                                    component={Listing}
                                />
                                <Route
                                    exact
                                    path={LISTING + "/:slug" + ID}
                                    component={ListingPage}
                                />
                                <Route
                                    exact
                                    path={PASSWORD_RESET}
                                    component={PasswordResetPage}
                                />
                                <Route
                                    exact
                                    path={PASSWORD_RESET + UPDATE + TOKEN}
                                    component={UpdatePasswordPage}
                                />
                                <Route
                                    exact
                                    path={PASSWORD_RESET + TOKEN}
                                    component={Token}
                                />
                                <Route
                                    exact
                                    path={ACTIVATE + TOKEN}
                                    component={Token}
                                />
                                <GuardRoute
                                    role="user"
                                    exact
                                    permission={["view listing"]}
                                    path="/test"
                                    component={Test}
                                />
                                <Route
                                    exact
                                    path={TERMS}
                                    component={TermsOfService}
                                />
                                <Route
                                    exact
                                    path="/privacy-policy"
                                    component={PrivacyPolicy}
                                />
                                <ProtectedRoute
                                    exact
                                    path={ACCOUNT}
                                    component={Account}
                                />
                                <ProtectedRoute
                                    exact
                                    path={SAVED}
                                    component={Saved}
                                />
                                <ProtectedRoute
                                    path={OVERVIEW}
                                    component={OverviewRoute}
                                />

                                {isUserAuthenticated() ? (
                                    <Route
                                        exact
                                        path={LOGIN}
                                        render={() => (
                                            <Redirect to={OVERVIEW} />
                                        )}
                                    />
                                ) : (
                                    <Route
                                        exact
                                        path={LOGIN}
                                        component={LoginPage}
                                    />
                                )}
                                {isUserAuthenticated() ? (
                                    <Route
                                        exact
                                        path={SIGN_UP}
                                        render={() => (
                                            <Redirect to={OVERVIEW} />
                                        )}
                                    />
                                ) : (
                                    <Route
                                        exact
                                        path={SIGN_UP}
                                        component={SignUpPage}
                                    />
                                )}
                                {isUserAuthenticated() ? (
                                    <Route
                                        exact
                                        path={HOME}
                                        render={() => (
                                            <Redirect to={OVERVIEW} />
                                        )}
                                    />
                                ) : (
                                    <Route exact path={HOME} component={Home} />
                                )}
                                <Route component={NotFound} />
                            </Switch>

                            <ToastContainer
                                position="top-left"
                                transition={Slide}
                                hideProgressBar={true}
                            />
                        </Suspense>
                    </div>
                </AppProvider>
            </ErrorBoundary>
        );
    }
}

export default App;

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <!--CSP-->
        <meta http-equiv="Content-Security-Policy"
        content="
        default-src 'unsafe-inline' 'self';
        script-src data: 'self' https://static.cloudflareinsights.com/ https://ajax.cloudflare.com https://sentry.io https://www.google-analytics.com/analytics.js https://client.crisp.chat https://client.crisp.chat/l.js https://settings.crisp.chat;
        script-src-elem 'self' 'unsafe-inline' https://static.cloudflareinsights.com/ https://ajax.cloudflare.com https://sentry.io https://client.crisp.chat/l.js https://www.google-analytics.com/analytics.js https://settings.crisp.chat https://client.crisp.chat;
        connect-src 'self' https//www.enproperty.co wss://stream.relay.crisp.chat wss://client.relay.crisp.chat https://sentry.io https://client.crisp.chat;
        img-src 'self' data: blob: https://www.google-analytics.com https://source.unsplash.com https://images.unsplash.com https://image.crisp.chat https://client.crisp.chat;
        style-src 'self' 'unsafe-inline' https://unpkg.com/balloon-css/ https://fonts.googleapis.com https://client.crisp.chat;
        font-src 'self' https://fonts.gstatic.com https://client.crisp.chat;
        frame-src 'self' https://player.vimeo.com https://www.youtube.com ;
        object-src 'self' data: blob:;
        worker-src 'self' blob: https://enproperty.co http://localhost:8000
        ">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0 shrink-to-fit=no" />
        <title>{{ config('app.name') }}</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="manifest" href="/manifest.json">
        {{-- Web App --}}
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="application-name" content="Enproperty">
        <meta name="apple-mobile-web-app-title" content="Enproperty">
        <meta name="theme-color" content="#C51350">
        <meta name="msapplication-navbutton-color" content="#C51350">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta name="msapplication-starturl" content="/">
        <meta name="google-site-verification" content="kj5b5f93xgmHJKu66okAJHD9uubN0jQ-Cc_Pd3rHAhk" />

        <link rel='mask-icon' href='/images/icons/safari-pinned-tab.svg' color='#C51350'>
        <link rel="icon" type="image/png" sizes="72x72" href="/images/icons/icon-72x72.png">
        <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="/images/icons/icon-72x72.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/images/icons/icon-96x96.png">
        <link rel="apple-touch-icon" type="image/png" sizes="96x96" href="/images/icons/icon-96x96.png">
        <link rel="icon" type="image/png" sizes="128x128" href="/images/icons/icon-128x128.png">
        <link rel="apple-touch-icon" type="image/png" sizes="128x128" href="/images/icons/icon-128x128.png">
        <link rel="icon" type="image/png" sizes="144x144" href="/images/icons/icon-144x144.png">
        <link rel="apple-touch-icon" type="image/png" sizes="144x144" href="/images/icons/icon-144x144.png">
        <link rel="icon" type="image/png" sizes="152x152" href="/images/icons/icon-152x152.png">
        <link rel="apple-touch-icon" type="image/png" sizes="152x152" href="/images/icons/icon-152x152.png">
        <link rel="icon" type="image/png" sizes="192x192" href="/images/icons/icon-192x192.png">
        <link rel="apple-touch-icon" type="image/png" sizes="192x192" href="/images/icons/icon-192x192.png">
        <link rel="icon" type="image/png" sizes="384x384" href="/images/icons/icon-384x384.png">
        <link rel="apple-touch-icon" type="image/png" sizes="384x384" href="/images/icons/icon-384x384.png">
        <link rel="icon" type="image/png" sizes="512x512" href="/images/icons/icon-512x512.png">
        <link rel="apple-touch-icon" type="image/png" sizes="512x512" href="/images/icons/icon-512x512.png">
        <meta name="rating" content="General">
        <!-- Fav -->
        {{-- <link rel="icon" href="favicon.ico" type="image/x-icon"/> --}}
        <link rel="icon" href="favicon-32x32" type="image/png"/>
        <link rel="icon" href="favicon.png" type="image/png"/>
        <!-- Styles -->
        <style>
                body {
                    margin: 0px;
                    background: var(--primary-color);
                }
                .noscroll {
                    overflow: hidden;
                }
                .blur {
                    filter: blur(10px);
                }
        </style>
        <link  href="/css/app.css" rel="stylesheet" as="style">
        <link  href="/css/import.css" rel="stylesheet" as="style">
        <link rel="stylesheet" href="https://unpkg.com/balloon-css/balloon.min.css">
    </head>
    <body id="body">
        <div id="modal"></div>
        <div id="root"></div>
        <script rel="preload" src="{{asset('js/app.js')}}" as="script"></script>
        {{--  <script defer src="/upup.min.js"></script>
        <script defer src="/upup.sw.min.js"></script>
        <script>
          UpUp.start({
            'cache-version': 'v0.9.?',
            'content-url': 'index.html'
            'assets': [
              'css/app.css',
              'css/import.css',
              'js/app.js',
              'manifest.json',
              'favicon.ico',
              'favicon-32x32.png',
            ]
          });
        </script>  --}}
        <noscript>
            <h1>javascript disabled</h1>
            <p>We need javascript to run this app</p>
        </noscript>
    </body>
</html>

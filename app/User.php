<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard_name = 'api';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'first_name',
        'last_name',
        'email', 'cell',
        'telephone',
        'password',
        'active',
        'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'password', 'remember_token', 'activation_token',
    ];

    public function properties()
    {
        return $this->hasMany('App\Property')->withCount('units');
    }

    public function units()
    {
        return $this->hasManyThrough('App\Unit', 'App\Property', 'user_id', 'property_id', 'id', 'id');
    }

    // public function likedProperty()
    // {
    //     return $this->morphedByMany('App\Property', 'likeable')->whereLiked(true);
    // }

    public function likedUnit()
    {
        return $this->morphedByMany('App\Unit', 'likeable')->whereLiked(true);
    }
}

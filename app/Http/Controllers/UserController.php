<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;
use App\Property;
use App\User;
use App\UnitImage;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return json
     */
    // public function show($id)
    // {
    //     return view('user.profile', ['user' => User::findOrFail($id)]);
    // }

    /**
     * Get the authenticated User's data
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $user = $request->user();
        $roles = $user->getRoleNames();
        return response()->json([
            "active"=>$user->active,
            "avatar"=>$user->avatar,
            "cell"=>$user->cell,
            "roles"=>$roles,
            "created_at"=>$user->created_at,
            "email"=>$user->email,
            "first_name"=>$user->first_name,
            "last_name"=>$user->last_name,
            "active"=>$user->active
        ]);
    }

    //Properties owned by user
    public function userProperties(Request $request)
    {
        $user = auth()->user();
        return response()->json($user->properties);
    }

    //Units owned by user
    public function userUnits(Request $request)
    {
        $user = auth()->user();
        $sub = $user->units->map(function ($unit) {
            $parent_property = Property::where('id', $unit->property_id)->first();
            $image_model = collect(UnitImage::where('unit_id', $unit['id'])->get());
            $images = $image_model->map(function ($image) {
                return [
                    'uuid' => $image['uuid'],
                    'url' => $image['image'],
                ];
            });

            return [
                'uuid' => $unit['uuid'],
                'unit_title' => $unit['unit_title'],
                'unit_description' => $unit['unit_description'],
                'price_fee' => $unit['price_fee'],
                'security_deposit' => $unit['security_deposit'],
                'listed' => $unit['listed'],
                'created_at' =>  $unit['created_at'],
                'updated_at' => $unit['updated_at'],
                'images' => $images,
                'parent_property' => [
                    'id' => $parent_property->uuid,
                    'property_title' => $parent_property->property_title,
                    'property_description' => $parent_property->property_description,
                    'street' => $parent_property->street,
                    'city' => $parent_property->city,
                    'state' => $parent_property->state,
                    'country' => $parent_property->country,
                ]
            ];
        });
        return $sub;
    }

    //properties liked by user
    // public function userLikedProperty(Request $request)
    // {
    //     $user_id = auth()->user()->id;
    //     $user = User::find($user_id);
    //     return response()->json($user->likedProperty);
    // }

    //Units liked by user
    public function userLikedUnit(Request $request)
    {
        $user = User::find(Auth::id());
        return response()->json($user->likedUnit);
    }


    public function update(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'first_name' => 'required|string|min:2|max:40',
            'last_name' => 'required|string|min:2|max:40',
            // 'email' => 'required|string|email|unique:users',
        ]);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        // $user->email = $request->email;

        $user->save();
        return response()->json([
            'message' => 'User has been updated!'
        ], 201);
    }


    public function avatar(Request $request)
    {
        $user = Auth::user();

        $avatar = $request->file('avatar');
        //Are their any files
        if (!empty($avatar)) {

            //Validate the avatar
            $this->validate($request, [
                'avatar.*' => 'image',
                'avatar.*size' => 'max:10000',
                'avatar.*.mimes' => 'mimes:jpg,jpeg,png,bmp,webmp'
            ]);

                //Check if there is an existing image then delete
                if($user->avatar != null){
                    $avatar_name = Str::after($user->avatar, 'storage/');
                    $path = 'public/'.$avatar_name;
                    Storage::delete($path);
                }

                //Store image to filesystem
                $path = Storage::put('public/avatar', $avatar,'public');
                $url = Storage::url($path);

                //Obtain the path to the image and save to user->avatar
                $user->avatar = $url;
                $user->save();

                //Return response to client
                $user->save();
                return response()->json([
                    'message' => 'User has been updated!'
                ], 201);

        }
        //Return an error message
        else {
            return response()->json([
                'message' => 'No image uploaded'
            ],404);
        }
    }
}

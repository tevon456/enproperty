<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\SmsVerification;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;

class SmsController extends Controller
{

    /**
     * Sends an sms message
     *
     */
    private function send_sms($to,$message){
        $client = new Client(env('TWILLIO_ACCOUNT_SID'), env('TWILLIO_AUTH_TOKEN'));
        $client->messages->create(
            // the number you'd like to send the message to
            $to,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => env('TWILLIO_NUMBER'),
                'body' => $message
            )
        );
    }

    /**
     * Generates a 5 digit one time password
     *
     */
    private function generate_otp(){
        return rand(100000,999999);
    }

    /**
     * Create verification request
     *
     * @return \Illuminate\Http\Response
     */
    public function verificaton_request(Request $request){
        $this->validate($request, [
            'contact_number' => 'required|string|max:12',
        ]);
        $sms = SmsVerification::where('user_id',Auth::id())->first();
        if($sms != null){
            //VERIFICATION ALREADY EXIST
            return $this->existing_sms_verification($sms ,$request);
        }else{
            //CREATE NEW SMS VERIFICATION
            return $this->new_sms_verification($request);
        }
    }

    public function verify_code(Request $request){

        $request->validate([
            // 'contact_number' => 'required|string|max:12',
            // 'code' => 'required|integer|max:6|min:6'
        ]);

        $sms = SmsVerification::where('user_id',Auth::id())->first();

        if($sms != null){
            $expired = $sms->updated_at->addMinutes(25);
            if($sms->status == 'verified'){
                //Number already verified
                return response()->json([
                    'message' => 'Phone number already verified'
                ]);
            }else if ($expired->isPast()) {
                return response()->json([
                    'message' => 'Code has expired try requesting a new code'
                ]);
            }else if($request->code != $sms->code){
                return response()->json([
                    'message' => 'This code is incorrect'
                ]);
            }else
                $sms->status = 'verified';
                $sms->code = null;
                $user = User::where('id',Auth::id())->first();
                $user->cell = $sms->contact_number;
                $user->save();
                if($sms->save()){
                return response()->json([
                    'message' => 'Phone number verified and saved'
                ]);}
        }
        else{
            return response()->json([
                'message' => 'Request for verification not found'
            ],404);
        }
    }

    private function new_sms_verification($request){
        $sms = new SmsVerification;
        $sms->contact_number = $request->input('contact_number');
        $sms->code = $this->generate_otp();
        $sms->user_id = Auth::id();
        $message = 'Enproperty verification code: '.$sms->code.' expires after 25 mins';
        $this->send_sms($sms->contact_number, $message);
        if($sms->save()){
            return response()->json([
                'message' => 'Check your phone for verification code'
            ]);
        }
    }

    private function existing_sms_verification($sms,$request){
        $time_sent = $sms->updated_at;
        $wait_time = $sms->updated_at->addMinutes(5);
        $expired = $sms->created_at->addMinutes(25);
        $contact = $request->contact_number;

        if($sms->status == 'verified' && $sms->contact_number == $contact){
            //Number already verified and not a new number
            return response()->json([
                'message' => 'Phone number already verified'
            ]);
        }else if(!$wait_time->isPast()){
            //5 minutes has not yet past before trying request
            return response()->json([
                'message' => 'Please wait 5 minutes before sending another request'
            ]);
        }else if($wait_time->isPast() && $sms->contact_number != $contact){
            //If number that comes in is different then update to the new number
            $sms->contact_number = $contact;
            $sms->code = $this->generate_otp();
            if($sms->save()){
                $message = 'Enproperty verification code: '.$sms->code.'';
                $this->send_sms($sms->contact_number, $message);
                return response()->json([
                    'message' => 'Check your phone for verification message'
                ]);
            }
        }else if($wait_time->isPast() && !$expired->isPast()){
            //5 minutes passed and code not expired send existing code
            $message = 'Enproperty verification code: '.$sms->code.' expires after 25 mins';
            $this->send_sms($sms->contact_number, $message);
            return response()->json([
                'message' => 'Check your phone for verification message '
            ]);
        }
        else if($wait_time->isPast() && $expired->isPast()){
            //5 mins passed and code has expired generate a new code and send it
            $sms->code = $this->generate_otp();
            if($sms->save()){
                $message = 'Enproperty verification code: '.$sms->code.' expires after 25 mins';
                $this->send_sms($sms->contact_number, $message);
                return response()->json([
                    'message' => 'Check your phone for verification message '
                ]);
            }
        }
    }





}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Unit;
use App\User;
use App\UnitImage;

class UnitImageController extends Controller
{

    /**
     * Validate Image files
     *
     */
    public function validateFiles(Request $request)
    {
        // dd($request->files);
        $files = $request->file('file');
        if (!empty($files)) {
            $this->validate($request, [
                'file.*' => 'image',
                'file.*size' => 'max:10000',
                'file.*.mimes' => 'mimes:jpg,jpeg,png,bmp,webmp'
            ]);

            //find unit
            $unit = Unit::where('uuid', $request->id)->first();
            if ($unit == null) {
                return response()->json([
                    'message' => 'not found'
                ], 404);
            }

            //find property that unit belongs to
            $property = DB::table('properties')->where('id', $unit->property_id)->first();
            //find owner of the property the unit belongs to
            $owner = DB::table('users')->where('id', $property->user_id)->first();
            //get current authenticated user
            $user = auth()->user()->id;

            //Check if user owns property
            if ($user != $owner->id) {
                return response()->json([
                    'message' => 'You dont own this Unit'
                ], 401);
            }

            //Start working on files and store them
            $imageDirName = $unit->uuid;
            foreach ($request->file('file') as $file) {
                $image = new UnitImage;
                $path = Storage::put('public/' . $imageDirName, $file, 'public');
                $url = Storage::url($path);
                $image->uuid = Str::uuid();
                $image->unit_id = $unit->id;
                $image->image = $url;
                $image->save();
            }
            if ($image->save()) {
                return response()->json([
                    'message' => $image
                ]);
            }
        } else {
            return response()->json([
                'message' => 'No files'
            ]);
        }
    }


    /**
     * Store images
     */
    public function store(Request $request)
    {
        return self::validateFiles($request);
    }

    /**
     * Delete images
     */
    public function destroy(Request $request)
    {

        $model = $this->getModels($request->id);
        if (auth()->user()->id != $model['owner']->id) {
            return response()->json([
                'message' => 'You dont own this Unit'
            ], 401);
        }

        $imageDirName = $model['unit']->uuid;
        $image_name = Str::after($model['image']->image, 'storage/'.$imageDirName.'/');
        $path = 'public/'.$imageDirName . '/'. $image_name;
        Storage::delete($path);
        $model['image']->delete();
        return response()->json([
            'message' => 'Image succesfully removed'
        ], 200);
    }

    private function getModels($uuid)
    {
        //get Image model
        $image = UnitImage::where('uuid', $uuid)->first();
        //get Unit
        $unit = Unit::findOrFail($image->unit_id);
        //find property that unit belongs to
        $property = DB::table('properties')->where('id', $unit->property_id)->first();
        //find owner of the property the unit belongs to
        $owner = DB::table('users')->where('id', $property->user_id)->first();
        //get current authenticated user
        return [
            'unit' => $unit,
            'image' => $image,
            'owner' => $owner
        ];
    }
}

<?php

namespace App\Http\Controllers;

use App\Notifications\SignupActivate;
use App\User;
use Config;
use Socialite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

// use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{

    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 900;
    /**
     * Create user
     * @param  [string] last_name
     * @param  [string] first_name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed|min:8',
            'role' => 'required|string|max:9'
        ]);

        $user = new User([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'activation_token' => str_random(60), //token used for email activation
        ]);

        $user->save();
        //Assign a role
        $this->assignRole($user, $request->role);
        //Send email for account activation
        $user->notify(new SignupActivate($user)); //Notify

        return response()->json([
            'message' => 'Successfully created user, check your email for account activation',
        ], 201);
    }

    /**
     * Assigns a role of either landlord or user to users signing up
     *
     */
    public function assignRole($user, $role)
    {
        if ($role == 'landlord') {
            $user->assignRole('landlord');
        } else if ($role == 'user') {
            $user->assignRole('user');
        } else {
            $user->assignRole('user');
        }
    }

    /**
     * Verifies token sent via email and then actiavtes the user's account if valid
     *
     * @return [string] message
     */
    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.',
            ], 401);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        return response()->json([
            'message' => 'Account activated you can now login',
        ], 200);
    }

    /**
     * Resends activation token via email if user isnt activated but exist
     *
     * @return [string] message
     */
    public function resendActivation($email)
    {
        $email->validate([
            'email' => 'required|string|email',
        ]);

        $user = User::where('email', $email)->where('activation_token', '!=', '')->first();
        if (!$user) {
            return response()->json([
                'message' => 'Request Received check your email',
            ], 200);
        }
        $user->notify(new SignupActivate($user));
        return response()->json([
            'message' => 'Request Received check your email',
        ], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean',
        ]);

        $credentials = request(['email', 'password']);
        $credentials['active'] = (1 or true);
        $credentials['deleted_at'] = null;

        $user = User::whereEmail($request['email'])->first();

        //Check if verified
        if ($user == null)
        // $this->incrementLoginAttempts($request);
        {
            return response()->json([
                'message' => 'Username or password is incorrect',
            ], 401);
        }

        if ($user->active == false or $user->active == 0) {
            return response()->json([
                'message' => 'You have not verified your account as yet, check your email for verification link',
            ], 401);
        }

        //Check if incorrect
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Username or password is incorrect',
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $this->createUserToken($user, $request);

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
        ]);
    }

    /**
     * Creates a token for a user
     *
     * @return [object] tokenResult
     */
    private function createUserToken($user, $request)
    {
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addDays(18);
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(4);
        }
        $token->save();

        return $tokenResult;
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }

    // /**
    //  * Redirect the user to the facebook authentication page.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function redirectToProvider()
    // {
    //     return Socialite::driver('facebook')->stateless()->user();
    // }

    // /**
    //  * Obtain the user information from facebook.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function handleProviderCallback()
    // {
    //     $user = Socialite::driver('facebook')->user();
    //     dd($user);
    // }

    // /**
    //  * Obtain the user information from facebook.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function handleProviderCallback()
    // {
    //     try {
    //         $user = Socialite::driver('facebook')->user();
    //     } catch (\Exception $e) {
    //         return redirect('/login');
    //     }

    //     // check if they're an existing user
    //     $existingUser = User::where('email', $user->email)->first();
    //     if($existingUser){
    //         // log them in
    //         auth()->login($existingUser, true);
    //     } else {
    //         // create a new user
    //         $newUser                  = new User;
    //         $newUser->name            = $user->name;
    //         $newUser->email           = $user->email;
    //         $newUser->facebook_id       = $user->id;
    //         $newUser->avatar          = $user->avatar;
    //         $newUser->avatar_original = $user->avatar_original;
    //         $newUser->save();
    //         auth()->login($newUser, true);
    //     }
    //     return redirect()->to('/');
    // }

}

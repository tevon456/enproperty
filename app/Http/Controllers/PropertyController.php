<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use App\Property;
use App\User;
use App\UnitImage;
use Illuminate\Support\Facades\Storage;
use App\Unit;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Property as PropertyResource;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get properties
        $properties = Property::paginate(40);
        // Return collection of properties as a resource
        return PropertyResource::collection($properties);
    }

    /**
     * Store a newly created resource in storage or update an existing one.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'property_title' => 'required|string|max:128',
            'property_description' => 'nullable|string|max:500',
            'street' => 'required|string|max:60',
            'city' => 'nullable|string|max:60',
            'state' => 'required|string|max:60',
            'country' => 'nullable|string|max:60'
        ]);

        $total_count = Property::where('user_id', Auth::id())->get();

        if($total_count->count() == 1){
            return response()->json([
                'message' => 'You cant add anymore properties'
            ], 403);
        }

        $property_from_uuid = Property::where('uuid',$request->property_id)->first();
        $property = $request->isMethod('put') ? Property::findOrFail($property_from_uuid->id)
        : new Property;

        $property->id = $request->input('property_id');
        $property->uuid = Str::uuid();
        $property->property_title = $request->input('property_title');
        $property->property_description = $request->input('property_description');
        $property->street = $request->input('street');
        $property->city = $request->input('city');
        $property->state = $request->input('state');
        $property->country = $request->input('country');
        $property->user_id = auth()->user()->id;

        if($property->save()) {

            // return new PropertyResource($property);
            return response()->json([
                'message' => 'You cant add anymore properties'
            ], 201);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Get property
        $property = Property::where('uuid',$id)->first();
        if($property == null){
            return response()->json([
                'message' => 'Not found'
            ], 404);
        }
        return new PropertyResource($property);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'property_title' => 'required|string|max:128',
            'property_description' => 'nullable|string|max:500',
            'street' => 'required|string|max:50',
            'city' => 'required|string|max:50',
            'state' => 'nullable|string|max:50',
            'country' => 'nullable|string|max:50'
        ]);

        $property = Property::where('uuid',$request->property_id)->first();
        $owner = DB::table('users')->where('id', $property->user_id)->first();
        $user = auth()->user()->id;

        if($user != $owner->id)
            return response()->json([
                'message' => 'You dont own this property'
            ], 401);

        // $property = Property::findOrFail($id);
        $property->property_title = $request->input('property_title');
        $property->property_description = $request->input('property_description');
        $property->street = $request->input('street');
        $property->city = $request->input('city');
        $property->state = $request->input('state');
        $property->country = $request->input('country');
        $property->save();

        if($property->save()) {
            return new PropertyResource($property);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::where('uuid',$id)->first();
        $owner = DB::table('users')->where('id', $property->user_id)->first();
        $user = auth()->user()->id;

        if($user != $owner->id)
            return response()->json([
                'message' => 'You dont own this property'
            ], 401);


        $delete_units = Unit::where('property_id', $property->id)->get();

        foreach ($delete_units as $unit) {
            $unit_images = UnitImage::where('unit_id',$unit->id)->get();
            foreach ($unit_images as $image) {
                $image_name = Str::after($image->image, 'storage/');
                $path = 'public/'.$image_name;
                Storage::delete($path);
                $image->delete();
            }

        }
        foreach ($delete_units as $unit_to_delete) {
            $unit_to_delete->delete();
        }

        if($property->delete()){
            return new PropertyResource($property);
        }
    }

}

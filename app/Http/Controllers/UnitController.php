<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Unit;
use App\UnitImage;
use App\User;
use App\Property;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Unit as UnitResource;



class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get units
        $units = Unit::where('listed', true)->orderBy('updated_at', 'desc')->paginate(30);
        $units->setPath('/');
        // $units->setBaseUrl('https://' . Request::getHttpHost() . '/' . Request::path());
        // Return collection of units as a resources
        return UnitResource::collection($units);
    }

    /**$units = Unit::where('listed', true)->orderBy('updated_at', 'desc')->paginate(30);
        $units->setBaseUrl('https://' . Request::getHttpHost() . '/' . Request::path());
     * Store a newly created resource in storage or update an existing one.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $parent_property = Property::where('user_id', Auth::id())->first();
        $unit_count = Unit::where('property_id',$parent_property->id)->get();

        if(auth()->user()->cell == null){
            return response()->json([
                'message' => 'A valid phone number is required before adding units, go to account and add a number'
            ], 401);
        }else if($unit_count->count() >= 3){
            return response()->json([
                'message' => 'You cant add anymore units'
            ], 403);
        }

        $this->validate($request, [
            'unit_title' => 'required|string|max:128',
            'unit_description' => 'nullable|string',
            'price_fee' => 'required|numeric|gt:0',
            'security_deposit' => 'nullable|numeric|gte:0',
            'listed' => 'required|boolean',
        ]);

        $unit = new Unit;
        $unit->id = $request->input('unit_id');
        $unit->uuid = Str::uuid();
        $unit->unit_title = $request->input('unit_title');
        $unit->unit_description = $request->input('unit_description');
        $unit->price_fee = $request->input('price_fee');
        $unit->security_deposit = $request->input('security_deposit');
        $unit->listed = $request->input('listed');

        //Check if the property id belongs to user before creating a unit
        $property = DB::table('properties')->where('uuid', $request->property_id)->first();
        //find owner of the property the unit belongs to
        $owner = DB::table('users')->where('id', $property->user_id)->first();
        //get current authenticated user
        $user = auth()->user();
        if ($user->id != $owner->id) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $unit->property_id = $property->id;

        if ($unit->save()) {
            // return new UnitResource($unit);
            return response()->json(
                $unit->uuid
            , 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Get Unit
        $unit = Unit::where('uuid', $id)->first();
        if ($unit == null) {
            return response()->json([
                'message' => 'Not found'
            ], 404);
        } elseif ($unit->listed == false) {
            return response()->json([
                'message' => 'Not found'
            ], 404);
        }
        return new UnitResource($unit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'unit_title' => 'required|string|max:128',
            'unit_description' => 'nullable|string',
            'price_fee' => 'required|numeric|gt:0',
            'security_deposit' => 'nullable|numeric|gte:0.0',
            'listed' => 'required|boolean',
            'unit'
        ]);

        //find unit
        $unit = Unit::where('uuid', $request->unit_id)->first();
        //find property that unit belongs to
        $property = DB::table('properties')->where('id', $unit->property_id)->first();
        //find owner of the property the unit belongs to
        $owner = DB::table('users')->where('id', $property->user_id)->first();
        //get current authenticated user
        $user = auth()->user()->id;

        if ($user != $owner->id)
            return response()->json([
                'message' => 'You dont own this Unit'
            ], 401);

        $unit->unit_title = $request->input('unit_title');
        $unit->unit_description = $request->input('unit_description');
        $unit->price_fee = $request->input('price_fee');
        $unit->security_deposit = $request->input('security_deposit');
        $unit->listed = $request->input('listed');
        $unit->save();

        if ($unit->save()) {
            return response()->json(
                $unit->uuid
            , 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find unit
        $unit= Unit::where('uuid', $id)->first();
        //find property that unit belongs to
        $property = DB::table('properties')->where('id', $unit->property_id)->first();
        //find owner of the property the unit belongs to
        $owner = DB::table('users')->where('id', $property->user_id)->first();
        //get current authenticated user
        $user = auth()->user()->id;

        if ($user != $owner->id)
            return response()->json([
                'message' => 'You dont own this Unit'
            ], 401);

        $directory = 'public/'.$unit->uuid;
        Storage::deleteDirectory($directory);
        foreach ($unit->images as $image){
            $image->delete();
        }

        if ($unit->delete()) {
            return new UnitResource($unit);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Like;
use App\Unit;

class LikeController extends Controller
{

    //! Disabling property liking for now
    // public function likeProperty($id)
    // {
    //     return $this->handleLike('App\Property', $id);
    // }

    public function likeUnit($uuid)
    {
        // here you can check if property exists or is valid or whatever
        //get the unit by its uuid
        $unit = Unit::where([['uuid',$uuid],['listed',true]])->first();
        if($unit == null){
            return response()->json([
                'message' => 'Not found'
            ], 404);
        }
        return $this->handleLike('App\Unit', $unit->id);

    }

    public function handleLike($type, $id)
    {

        $like = Like::withTrashed()->firstOrCreate([
            'user_id'       => Auth::id(),
            'likeable_id'   => $id,
            'likeable_type' => $type
        ]);

        if($like->liked == null || $like->liked == false){
            $like->liked = true;
            $like->save();
            return response()->json([
                'liked' => $like->liked
            ]);

        }else{
            $like->liked = false;
            $like->save();
            // return $like->liked;
            return response()->json([
                'liked' => $like->liked
            ]);
        }
    }

}



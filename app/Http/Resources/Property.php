<?php

namespace App\Http\Resources;

use App\User;

use Illuminate\Http\Resources\Json\JsonResource;

class Property extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        // $property = $this->makeVisible('user_id')->toArray();
        $property = $this->withCount('units')->get()->makeVisible('user_id')->toArray();
        $user = User::where('id', $this->user_id)->first();
        // dd($property);

        // return $with_units_count;
        return
            [
                'id' => $this->uuid,
                'property_title' => $this->property_title,
                'property_description' => $this->property_description,
                'street' => $this->street,
                'city' => $this->city,
                'state' => $this->state,
                'country' => $this->country,
                'is_liked' => $this->is_liked,
                'owner_first_name' => $user->first_name,
                'owner_last_name' => $user->last_name,
                'owner_avatar'=>$user->avatr,
            ];
    }
}

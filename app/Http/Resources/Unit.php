<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use App\Property;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\UnitImage;

class Unit extends JsonResource
{

    /**
     * Appends parent resource unit resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function appendImages($request)
    {

        $image_model = collect(UnitImage::where('unit_id', $this->id)->get());
        // dd($image_model);
        $images = $image_model->map(function ($image) {
            return [
                'uuid' => $image['uuid'],
                'url' => $image['image'],
            ];
        });
        return $images;
    }

    /**
     * Appends parent resource unit resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function appendParent($request)
    {
        $parent_property = Property::where('id', $this->property_id)->first();
        $owner = User::where('id', $parent_property->user_id)->first();

        if(Auth::check()){
            $cell = $owner->cell;
            $telephone = $owner->telephone;
        }else{
            $cell = null;
            $telephone = null;
        }

        if ($this->property_id == null) {
            return null;
        } else {
            return [
                'id' => $parent_property->uuid,
                'property_title' => $parent_property->property_title,
                'property_description' => $parent_property->property_description,
                'street' => $parent_property->street,
                'city' => $parent_property->city,
                'state' => $parent_property->state,
                'country' => $parent_property->country,
                'owner_first_name' => $owner->first_name,
                'owner_last_name' => $owner->last_name,
                'owner_avatar'=> $owner->avatar,
                'owner_cell'=>$cell,
                'owner_telephone'=>$telephone
            ];
        }
    }


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'id' => $this->uuid,
                'is_liked' => $this->is_liked,
                'unit_title' => $this->unit_title,
                'unit_description' => $this->unit_description,
                'price_fee' => $this->price_fee,
                'security_deposit' => $this->security_deposit,
                'listed' => $this->listed,
                'parent_property' => self::appendParent($request),
                'images' => self::appendImages($request),
            ];
    }
}

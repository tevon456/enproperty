<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{

    use SoftDeletes;

    protected $table = 'likeables';

    protected $fillable = [
        'user_id',
        'likeable_id',
        'likeable_type',
        'liked'
    ];

    /**
     * Get all of the properties that are assigned this like.
     */
    public function property()
    {
        return $this->morphedByMany('App\Property', 'likeable');
    }

    /**
     * Get all of the properties that are assigned this like.
     */
    public function unit()
    {
        return $this->morphedByMany('App\Unit', 'likeable');
    }
}

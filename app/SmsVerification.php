<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsVerification extends Model
{
    // protected $fillable = [
    //     'email', 'token'
    // ];

    protected $fillable = [
        'user_id',
        'contact_number',
    ];
}

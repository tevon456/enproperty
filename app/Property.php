<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Property extends Model
{
    protected $appends = ['is_liked'];
    protected $hidden = [
        'id', 'user_id', 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function units()
    {
        return $this->hasMany('App\Unit');
    }

    public function likes()
    {
        //TODO fix this all of this madness
        return $this->morphToMany('App\User', 'likeable')->where('likeables.liked', '=', true);
    }

    public function getIsLikedAttribute()
    {
        $like = $this->likes()->where('user_id', auth::id())->first();
        return (!is_null($like)) ? true : false;
    }
}

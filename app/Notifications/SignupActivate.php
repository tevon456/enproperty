<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SignupActivate extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
            //Actual api end
            // $url = url('/api/auth/signup/activate/'.$notifiable->activation_token);
            //intermediate react auth component will pickup this route

            $url2 = url("/activate/".$notifiable->activation_token);
            return (new MailMessage)
                ->subject('Confirm your account')
                ->line('Hi ,')
                ->line('To complete your sign up, please verify your email:')
                // ->action('Confirm Account', url($url))
                ->action('Confirm Account', url($url2))
                ->line('Thank you for using our application, if you did not sign up for an account on https://enproperty.co ignore and delete this email.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

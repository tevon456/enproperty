<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitImage extends Model
{
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }
}

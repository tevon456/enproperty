<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/login', function () {
    return response()->json([
        'message' => 'Unauthorized'
    ], 401);
})->name("login");

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Password reset routes
Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password' //eg api/password/create [api route for password reset]
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});

//Passport auth routes
Route::group([
    'prefix' => 'auth',// eg/api/auth/login
    'middleware' => 'api',
    'middleware' => 'throttle:20,5'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate');
    Route::get('signup/resend-activation', 'AuthController@resendActivation');
    Route::group([
        'middleware' => 'auth:api' //only api authorised user may access
    ], function () {
        Route::get('logout', 'AuthController@logout');
    });
});

//API v1.01 these are more structured api endoints
Route::group(['prefix' => 'v1.01', 'middleware' => 'api'], function () {

    //USER AUTH
    Route::group([
        'prefix' => '/auth/user',
        'middleware' => ['auth:api'],
    ], function () {
        // Logged in user's data
        Route::get('/', 'UserController@user');
        // Update user's data
        Route::patch('/update', 'UserController@update');
        // Update user's avatar
        Route::post('/avatar', 'UserController@avatar');
        // Returns the units that the user likes
        Route::get('/like', 'UserController@userLikedUnit');
        //Send request to be verified
        Route::post('/verify/request', 'SmsController@verificaton_request')->middleware('role:admin|landlord');
        //Send code to be verified
        Route::post('/verify/code', 'SmsController@verify_code')->middleware('role:admin|landlord');
        // Logged in user's owned properties
        Route::get('/property', 'UserController@userProperties')->middleware('role:admin|landlord');
        // Logged in user's owned units
        Route::get('/units', 'UserController@userUnits')->middleware('role:admin|landlord');
    });

    //UNIT
    Route::group([
        'prefix' => 'units'
    ], function () {
        // Show all units
        Route::get('/', 'UnitController@index');
        // Show single unit
        Route::get('/{id}', 'UnitController@show');
    });

    //UNIT AUTH
    Route::group([
        'prefix' => 'auth/units', 'middleware' => ['auth:api'],
    ], function () {
        // Show all units + protected fields
        Route::get('/', 'UnitController@index');
        // Show single unit + protected fields
        Route::get('/{id}', 'UnitController@show');
        //Like a property using its uuid
        Route::get('/like/{id}', ['as' => 'unit.like', 'uses' => 'LikeController@likeUnit']);
        // Create new unit
        Route::post('/', 'UnitController@store')->middleware('permission:create listing');
        //Store images for unit
        Route::post('/image', 'UnitImageController@store')->middleware('permission:create listing');
        //Delete a unit image
        Route::delete('/image', 'UnitImageController@destroy')->middleware('permission:delete listing');
        // Update unit data
        Route::patch('/update', 'UnitController@update')->middleware('permission:edit listing');
        // Delete unit
        Route::delete('/{id}', 'UnitController@destroy')->middleware('permission:delete listing');
    });

    //PROPERTY
    Route::group([
        'prefix' => 'property'
    ], function () {
        // Show all property
        Route::get('/', 'PropertyController@index');
        // Show single property
        Route::get('/{id}', 'PropertyController@show');
    });

    //PROPERTY AUTH
    Route::group([
        'prefix' => 'auth/property', 'middleware' => ['auth:api']
    ], function () {
        // Show all property + protected fields
        Route::get('/', 'PropertyController@index');
        // Show single property + protected fields
        Route::get('/{id}', 'PropertyController@show');
        // Create new property
        Route::post('/', 'PropertyController@store')->middleware('permission:create property');
        // Update unit data
        Route::patch('/update', 'PropertyController@update')->middleware('permission:edit property');
        // Delete a single property
        Route::delete('/{id}', 'PropertyController@destroy')->middleware('permission:delete property');
    });
});
